<?php
require_once("../../class/Include.class.php");
require_once("Venda_Model.php");
require_once("../trava.php");

$obj_control = new Venda_Model();
$obj_site = new Site();



if(isset($_POST[action]))
{
      $obj_control->altera($_POST[id], $_POST);
      Util::script_msg("Dados alterado com sucesso.");
      Util::script_location("index.php");
}





$dados = $obj_control->select_dentro($_GET[id]);



$dados_usuario = $obj_site->select_unico("tb_usuarios", "idusuario", $dados[id_usuario]);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>

<?php require_once("../includes/head.php") ?>

<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>



  
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
<script>
$(function() {
    $("#data_entrega, #data_separacao_entrega, #data_entrega_cliente").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: 0,
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
    });

    // desabilitando a digitação do input
    $("#data_entrega, #data_separacao_entrega, #data_entrega_cliente").attr( 'readOnly' , 'true' );

});
</script>



</head>

<body>


	<!-- ======================================================================= -->
	<!-- topo	-->
	<!-- ======================================================================= -->
	<?php require_once("../includes/topo.php"); ?>





	<div class="container-fluid">
      <div class="row">


      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-2 sidebar">
      	<?php require_once("../includes/lateral.php"); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->





      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-10 main ">
            
          
          <?php echo utf8_encode($dados[msg_email]); ?>

          <div class="clearfix"></div>

          <div class="top40 bottom40"></div>


          <form action="" method="post" name="form_listagem" id="form_listagem" enctype="multipart/form-data">
               
              <div class="col-xs-4">
                <div class="form-group">
                  <label for="exampleInputName2">Data do pagamento</label>
                  <input type="text" class="form-control" name="data_entrega" id="data_entrega" value="<?php echo Util::formata_data($dados[data_entrega]) ?>" >
                </div>
              </div>  
              
              <div class="col-xs-4">
                <div class="form-group">
                  <label for="exampleInputEmail2">Data de separação para entrega</label>
                  <input type="text" class="form-control" name="data_separacao_entrega" id="data_separacao_entrega" value="<?php echo Util::formata_data($dados[data_separacao_entrega]) ?>" >
                </div>
              </div>
              
              <div class="col-xs-4">
                <div class="form-group">
                  <label for="exampleInputEmail2">Data de entrega ao cliente</label>
                  <input type="text" class="form-control" name="data_entrega_cliente" id="data_entrega_cliente" value="<?php echo Util::formata_data($dados[data_entrega_cliente]) ?>" >
                </div>
              </div>

              <div class="text-center">
                <button type="submit" name="action" class="btn btn-primary">Atualizar datas</button>
              </div>


              <input type="hidden" name="id" id="id" value="<?php echo $_GET[id]; ?>" />

          </form>



      </div>
      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->


        




</body>
</html>
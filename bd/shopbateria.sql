-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: shopbateria.mysql.dbaas.com.br
-- Generation Time: 20-Jun-2016 às 15:15
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopbateria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Index banner', '0906201611041319333581.jpg', 'SIM', NULL, '1', 'index-banner', '/produtos', NULL, NULL),
(2, 'Index Banner 1', '0906201611041129394326.jpg', 'SIM', NULL, '1', 'index-banner-1', '/produtos', NULL, NULL),
(3, 'Index Banner 2', '0906201603021341056304.jpg', 'SIM', NULL, '1', 'index-banner-2', '/produtos', NULL, NULL),
(4, 'Mobile index 01', '1306201601101280458940.jpg', 'SIM', NULL, '2', 'mobile-index-01', 'mobile/produtos', NULL, NULL),
(5, 'Mobile index 02', '1306201601111193376458.jpg', 'SIM', NULL, '2', 'mobile-index-02', '/produtos', NULL, NULL),
(6, 'Mobile index 03', '1306201601111349322069.jpg', 'SIM', NULL, '2', 'mobile-index-03', 'mobile/produtos', NULL, NULL),
(7, 'Produtos - Banner 1', '1806201601421305904352.jpg', 'SIM', NULL, '3', 'produtos--banner-1', '', NULL, NULL),
(8, 'Produtos - Banner 2', '1806201601421305904352.jpg', 'SIM', NULL, '3', NULL, '/produtos', NULL, NULL),
(9, 'Produtos - Banner 3', '1806201601421305904352.jpg', 'SIM', NULL, '3', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '0906201612061232825871.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Produtos', '0906201612091409408525.jpg', NULL, 'produtos', 'SIM', NULL),
(3, 'Produto dentro', '0906201612421126675565.jpg', NULL, 'produto-dentro', 'SIM', NULL),
(4, 'Orçamento', '1106201610371177282849.jpg', NULL, 'orcamento', 'SIM', NULL),
(5, 'Contatos', '0906201605121165887771.jpg', NULL, 'contatos', 'SIM', NULL),
(6, 'Trabalhe Conosco', '2405201606581339295495.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(7, 'Serviços', '0906201612421304042828.jpg', NULL, 'servicos', 'SIM', NULL),
(8, 'Servços Dentro', '0906201601091163004789.jpg', NULL, 'servcos-dentro', 'SIM', NULL),
(9, 'Dicas', '1006201604201285595392.jpg', NULL, 'dicas', 'SIM', NULL),
(10, 'Dica Dentro', '1006201604211122396708.jpg', NULL, 'dica-dentro', 'SIM', NULL),
(11, 'Produto Categoria', '1006201604201161435570.jpg', NULL, 'produto-categoria', 'SIM', NULL),
(12, 'Mobile Empresa', '1406201609521407346646.jpg', NULL, 'mobile-empresa', 'SIM', NULL),
(13, 'Mobile Dicas', '1406201610491372249076.jpg', NULL, 'mobile-dicas', 'SIM', NULL),
(14, 'Mobile Dica dentro', '1406201611211307272498.jpg', NULL, 'mobile-dica-dentro', 'SIM', NULL),
(15, 'Mobile Fale Conosco', '1406201612241308695742.jpg', NULL, 'mobile-fale-conosco', 'SIM', NULL),
(16, 'Mobile Trabalhe Conosco', '1406201601161329159441.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', NULL),
(17, 'Mobile Orcamentos', '1406201603381338775464.jpg', NULL, 'mobile-orcamentos', 'SIM', NULL),
(18, 'Mobile Produtos', '1406201604021328712396.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(19, 'Mobile Produtos Dentro', '1406201605471132003607.jpg', NULL, 'mobile-produtos-dentro', 'SIM', NULL),
(20, 'Mobile Serviços', '1506201608441385614800.jpg', NULL, 'mobile-servicos', 'SIM', NULL),
(21, 'Mobile Serviços Dentro', '1506201607361116256686.jpg', NULL, 'mobile-servicos-dentro', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_mensagens`
--

CREATE TABLE `tb_categorias_mensagens` (
  `idcategoriamensagem` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_mensagens`
--

INSERT INTO `tb_categorias_mensagens` (`idcategoriamensagem`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Mensagens de aniversário', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-aniversario', 'Mensagens de aniversário', 'Mensagens de aniversário', 'Mensagens de aniversário'),
(2, 'Mensagens de casamento', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-casamento', 'Mensagens de casamento', 'Mensagens de casamento', 'Mensagens de casamento'),
(3, 'Mensagens dia das mães', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-dia-das-maes', 'Mensagens dia das mães', 'Mensagens dia das mães', 'Mensagens dia das mães'),
(4, 'Mensagens de amor', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-amor', 'Mensagens de amor', 'Mensagens de amor', 'Mensagens de amor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'FREEDOM', '1706201601321116545746.png', 'SIM', NULL, 'freedom', NULL, NULL, NULL),
(2, 'YUASA', '1706201601321304924268.png', 'SIM', NULL, 'yuasa', NULL, NULL, NULL),
(3, 'OPTIMA BATTERIES', '1706201601321176500475.png', 'SIM', NULL, 'optima-batteries', NULL, NULL, NULL),
(4, 'OPTIMA BATTERIES 1', '1706201601321176500475.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(5, 'YUASA', '1706201601321304924268.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(6, 'FREEDOM', '1706201601321116545746.png', 'SIM', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`) VALUES
(1, '', '', '', 'SIM', 0, '', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Goiânia - GO', '1111-1111', '2222-2222', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122829.18590934838!2d-48.11783072819571!3d-15.834925803033512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a32f62027f279%3A0x9a336abb283db55a!2sExtra+Hiper!5e0!3m2!1spt-BR!2sbr!4v146222093908', NULL, NULL, 'marciomas@gmail.com', 'https://plus.google.com/', '3333-3333', '4444-4444', '(61)', '(61)', '(61)', '(61)', 'https://www.facebook.com/', 'https://www.twitter.com/');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'Dica 1 Lorem ipsum dolor sit amet lorem Lorem ipsum dolor sit amet loremLorem ipsum dolor sit amet loremLorem ipsum dolor sit amet lorem', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '1506201604211173524024..jpg', 'SIM', NULL, 'dica-1-lorem-ipsum-dolor-sit-amet-lorem-lorem-ipsum-dolor-sit-amet-loremlorem-ipsum-dolor-sit-amet-loremlorem-ipsum-dolor-sit-amet-lorem', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'Dica 2 Lorem ipsum dolor sit amet', '<div>\r\n	1 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.</div>\r\n<div>\r\n	&nbsp;&nbsp;</div>\r\n<div>\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</div>', '1506201604231242998419..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'Dica 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '1506201604241185284036..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'Dica 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '1506201604261302218156..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'Dica 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '1506201604261300439100..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'Dica 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '1506201604271157904830..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'Dicas Mobile Lorem ipsum dolor sit amet, consetetur', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '1506201604281287018636..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Empresa', '<p>\r\n	SHOPPING BATERIAS e especializado no com&eacute;rcio de baterias: automotivas, para motos, lanchas e estacion&aacute;rias.Tamb&eacute;m trabalhamos com servi&ccedil;os de auto-el&eacute;trica.<br />\r\n	Nossos clientes podem contar com profissionais eletricistas de autos com mais de 30 anos de experi&ecirc;ncia.SHOPPING BATERIAS e especializado no com&eacute;rcio de baterias: automotivas, para motos, lanchas e estacion&aacute;rias.Tamb&eacute;m trabalhamos com servi&ccedil;os de auto-el&eacute;trica.Nossos clientes podem contar com profissionais eletricistas de autos com mais de 30 anos de experi&ecirc;ncia.<br />\r\n	&nbsp;</p>', 'SIM', 0, '', '', '', 'empresa', NULL, NULL, NULL),
(2, 'Empresa - Tempo e formas de entrega', '<p>\r\n	Nossos produtos s&atilde;o entregues em ve&iacute;culos garantindo o perfeito estado de conserva&ccedil;&atilde;o das flores.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pedidos confirmados at&eacute; &agrave;s 12:00 horas poder&atilde;o ser entregues no mesmo dia, exceto em datas especiais. Ap&oacute;s esse hor&aacute;rio entregaremos seu pedido no dia seguinte ou na data especificada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Em datas especiais, recomendamos que os pedidos sejam feitos com 02(dois) dias de anteced&ecirc;ncia. Caso isso n&atilde;o ocorra, o pedido estar&aacute; sujeito a confirma&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Endere&ccedil;o da entrega:</p>\r\n<p>\r\n	Importante: Recomendamos que no endere&ccedil;o do destinat&aacute;rio tenha sempre algu&eacute;m para receber as flores. Caso isso n&atilde;o ocorra, ser&aacute; cobrado uma taxa de reentrega no valor de R$ 10,00. Este per&iacute;odo de espera pode, eventualmente, prejudicar a qualidade dos arranjos. Infelizmente, n&atilde;o podemos nos responsabilizar por poss&iacute;veis danos que o arranjo tenha sofrido por ter esperado muito tempo at&eacute; seu recebimento. Portanto, &eacute; preciso certificar-se de que haja algu&eacute;m para receber as flores no destinat&aacute;rio para que cheguem sempre vivas e frescas para a pessoa presenteada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Entregas em hor&aacute;rios especiais:</strong></p>\r\n<p>\r\n	(&Eacute; obrigat&oacute;rio a verifica&ccedil;&atilde;o de disponibilidade de hor&aacute;rio antes da contrata&ccedil;&atilde;o do servi&ccedil;o).</p>\r\n<p>\r\n	<strong>Segunda a sexta:</strong></p>\r\n<p>\r\n	- Entre 7h e as 18h as entregas s&atilde;o gratuitas de acordo com os per&iacute;odos de entregas citados acima</p>\r\n<p>\r\n	- Entre 18h e 20h as entregas s&atilde;o cobradas R$ 10,00 adicionais e o valor n&atilde;o &eacute; referente ao hor&aacute;rio marcado, mas somente a entrega em hor&aacute;rio especial.</p>\r\n<p>\r\n	- Entre 20h e 21h as entregas s&atilde;o cobradas R$ 20,00 adicionais e o valor n&atilde;o &eacute; referente ao hor&aacute;rio marcado, mas somente a entrega em hor&aacute;rio especial.</p>\r\n<p>\r\n	<strong>S&aacute;bado:</strong></p>\r\n<p>\r\n	- Entre 7h e as 12h as entregas s&atilde;o gratuitas de acordo com os per&iacute;odos de entregas citados acima</p>\r\n<p>\r\n	- Entre 12h e 18h as entregas s&atilde;o cobradas R$ 10,00 adicionais e o valor n&atilde;o &eacute; referente ao hor&aacute;rio marcado, mas somente a entrega em hor&aacute;rio especial.</p>\r\n<p>\r\n	- Entre 18h e 20h as entregas s&atilde;o cobradas R$ 20,00 adicionais e o valor n&atilde;o &eacute; referente ao hor&aacute;rio marcado, mas somente a entrega em hor&aacute;rio especial</p>\r\n<p>\r\n	<strong>Domingos e feriados:</strong></p>\r\n<p>\r\n	- Entre 9h e 12h as entregas s&atilde;o cobradas R$ 10,00 adicionais e o valor n&atilde;o &eacute; referente ao hor&aacute;rio marcado, mas somente a entrega em hor&aacute;rio especial.</p>\r\n<p>\r\n	- Entre 12h e 15h as entregas s&atilde;o cobradas R$ 20,00 adicionais e o valor n&atilde;o &eacute; referente ao hor&aacute;rio marcado, mas somente a entrega em hor&aacute;rio especial</p>', 'SIM', 0, '', '', '', 'empresa--tempo-e-formas-de-entrega', NULL, NULL, NULL),
(3, 'Empresa - Formas de pagamento', '<p>\r\n	DEP&Oacute;SITO BANC&Aacute;RIO* OU TRANSFER&Ecirc;NCIA BANC&Aacute;RIA*</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para fazer o pagamento atrav&eacute;s de dep&oacute;sito em lot&eacute;rica ou transfer&ecirc;ncia banc&aacute;ria, favor verificar os dados do banco desejado:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Caixa Econ&ocirc;mica Federal</strong></p>\r\n<p>\r\n	Ag&ecirc;ncia: 3037 &nbsp;Opera&ccedil;&atilde;o: 003</p>\r\n<p>\r\n	Conta Corrente: &nbsp;1537-1</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Banco do Brasil - 001</strong></p>\r\n<p>\r\n	Ag&ecirc;ncia: Ag: 3486-X&nbsp;</p>\r\n<p>\r\n	Conta corrente: 11610-6</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Banco Bradesco - 237</strong></p>\r\n<p>\r\n	Ag&ecirc;ncia: 2241-1</p>\r\n<p>\r\n	Conta corrente: 17328-2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Banco Ita&uacute; - 341&nbsp;</strong></p>\r\n<p>\r\n	Ag&ecirc;ncia: 8788</p>\r\n<p>\r\n	Conta Poupan&ccedil;a: 04791-1/500</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A libera&ccedil;&atilde;o do pedido s&oacute; se dar&aacute; mediante a comprova&ccedil;&atilde;o do pagamento atrav&eacute;s do envio do comprovante por e-mail.</p>\r\n<p>\r\n	<strong>Favor N&Atilde;O efetuar o pagamento atrav&eacute;s de terminais eletr&ocirc;nicos (envelopes).</strong></p>', 'SIM', 0, '', '', '', 'empresa--formas-de-pagamento', NULL, NULL, NULL),
(4, 'Promoções - Legenda', '<p>\r\n	teste 01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'SIM', 0, '', '', '', 'promocoes--legenda', NULL, NULL, NULL),
(5, 'Empresa - Home', '<p>\r\n	SHOPPING BATERIAS e especializado no com&eacute;rcio de baterias: automotivas, para motos, lanchas e estacion&aacute;rias.Tamb&eacute;m trabalhamos com servi&ccedil;os de auto-el&eacute;trica.<br />\r\n	Nossos clientes podem contar com profissionais eletricistas de autos com mais de 30 anos de experi&ecirc;ncia.SHOPPING BATERIAS e especializado no com&eacute;rcio de baterias: automotivas, para motos, lanchas e estacion&aacute;rias.Tamb&eacute;m trabalhamos com servi&ccedil;os de auto-el&eacute;trica.Nossos clientes podem contar com profissionais eletricistas de autos com mais de 30 anos de experi&ecirc;ncia.<br />\r\n	&nbsp;</p>', 'SIM', 0, '', '', '', 'empresa--home', NULL, NULL, NULL),
(6, 'Promoções', '<div>\r\n	A UNAFLOR (promocoes)&eacute; uma empresa especializada em ornamenta&ccedil;&atilde;o de flores, que trabalha com bom gosto e sofistica&ccedil;&atilde;o. Surgiu para mudar o conceito de floricultura em Goi&acirc;nia e oferecer os mais elaborados buqu&ecirc;s e arranjos, sempre contemplando a beleza natural das flores.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;A nossa floricultura &eacute; online e conta sempre com produtos e servi&ccedil;os de primeira qualidade.</div>', 'SIM', 0, '', '', '', 'promocoes', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentos`
--

CREATE TABLE `tb_equipamentos` (
  `idequipamento` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `imagem_interna` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_equipamentos`
--

INSERT INTO `tb_equipamentos` (`idequipamento`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`, `imagem_interna`) VALUES
(1, 'Equpamento 1 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Produto 1&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'SIM', 0, 'equpamento-1-lorem-ipsum-dolor-sit-amet', 80, '3M', NULL, NULL, '', '', 1, '0405201610431245857231..jpg'),
(2, 'Equpamento 2 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Porta postigo direita Viena Ouro&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>\r\n<p>\r\n	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'SIM', 0, 'produto-2-lorem-ipsum-dolor-sit-amet', 77, 'Tigre', NULL, NULL, '', '', 3, '0405201610431245857231..jpg'),
(7, 'Equpamento 3 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Fogos de Artif&iacute;cios 0&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'SIM', 0, 'produto-3-lorem-ipsum-dolor-sit-amet', 82, 'Amanco', NULL, NULL, '', '', 4, '0405201610431245857231..jpg'),
(8, 'Equpamento 4 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	4&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-4-lorem-ipsum-dolor-sit-amet', 80, 'Sol', NULL, NULL, '', '', 1, '0405201610431245857231..jpg'),
(9, 'Equpamento 5 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-5-lorem-ipsum-dolor-sit-amet', 81, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(10, 'Equpamento 6 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-6-lorem-ipsum-dolor-sit-amet', 77, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(11, 'Equpamento 7 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-7-lorem-ipsum-dolor-sit-amet', 77, NULL, NULL, NULL, 'https://www.youtube.com/embed/xIM22tZAdwE', '<p>\r\n	Apresenta&ccedil;&atilde;o do v&iacute;deo Lorem Ipsum is simply dummy text of the printing and typesettin g industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a gall ey of type and scrambled it to make a type specimen book. It h as survived not only five centuries, but alsothe leap into electro nic typesetting, remaining essentially unchanged. It was popularis ed in the 1960</p>', NULL, '0405201610431245857231..jpg'),
(12, 'Equpamento 8 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Produto 8 Lorem ipsum dolor sit amet&nbsp;Produto 8 Lorem ipsum dolor sit amet&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-8-lorem-ipsum-dolor-sit-amet', 75, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(13, 'Equpamento 9 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-9-lorem-ipsum-dolor-sit-amet', 74, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(14, 'Equpamento 1001 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-1001-lorem-ipsum-dolor-sit-amet', 82, NULL, NULL, NULL, '', '', 4, '0405201610431245857231..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_fretes`
--

CREATE TABLE `tb_fretes` (
  `idfrete` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(145) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_fretes`
--

INSERT INTO `tb_fretes` (`idfrete`, `titulo`, `uf`, `valor`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Taguatinga Sul', 'DF', 10.5, '', 0, 'taguatinga-sul'),
(2, 'Samambaia', '', 7.98, 'SIM', 0, 'samambaia'),
(3, 'Recanto das Emas', '', 15, 'SIM', 0, 'recanto-das-emas'),
(4, 'Samambaia Norte', '', 14.89, 'SIM', 0, 'samambaia-norte'),
(5, 'Asa Sul', '', 32.5, 'SIM', 0, 'asa-sul'),
(6, 'Asa Norte', '', 10.09, 'SIM', 0, 'asa-norte');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(1, '2006201609031293244669.jpg', 'SIM', NULL, NULL, 16),
(2, '2006201609031238360150.jpg', 'SIM', NULL, NULL, 16),
(4, '2006201609031210356624.jpg', 'SIM', NULL, NULL, 16),
(5, '2006201609031352651580.jpg', 'SIM', NULL, NULL, 16),
(6, '2006201609031284906217.jpg', 'SIM', NULL, NULL, 16),
(7, '2006201609031174507000.jpg', 'SIM', NULL, NULL, 16);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `super_admin`) VALUES
(1, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', 'NAO'),
(2, 'Amanda', 'b362cb319b2e525dc715702edf416f10', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:00:35', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:04:48', 1),
(3, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:07:05', 1),
(4, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:08:09', 1),
(5, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:29:16', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:36:51', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '23:43:19', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-03', '00:26:52', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-03', '12:55:52', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '13:49:39', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '22:40:32', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '22:43:25', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:23:17', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:23:48', 1),
(15, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '20:37:32', 1),
(16, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:48:58', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:53:28', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:09:54', 1),
(19, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:10:17', 1),
(20, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:10:38', 1),
(21, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:16:08', 1),
(22, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:17:58', 1),
(23, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '11:47:16', 1),
(24, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '15:03:28', 1),
(25, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '20:53:17', 1),
(26, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '12:39:07', 1),
(27, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:04:11', 1),
(28, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:06:54', 1),
(29, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:16:16', 1),
(30, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:16:48', 1),
(31, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:17:20', 1),
(32, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:19:58', 1),
(33, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:20:41', 1),
(34, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:26:01', 1),
(35, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:28:44', 1),
(36, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '21:46:41', 1),
(37, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '23:10:36', 1),
(38, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:12:58', 1),
(39, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:13:37', 1),
(40, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:16:09', 1),
(41, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:27:51', 1),
(42, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:30:13', 1),
(43, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:31:16', 1),
(44, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:33:18', 1),
(45, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:35:55', 1),
(46, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:36:18', 1),
(47, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '21:28:19', 1),
(48, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '20:26:00', 1),
(49, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '08:10:59', 1),
(50, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '12:36:13', 1),
(51, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '12:40:40', 1),
(52, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '13:32:27', 1),
(53, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '13:34:19', 1),
(54, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '10:41:41', 1),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '11:23:33', 1),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '14:00:50', 1),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:09:41', 1),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '15:16:27', 1),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:17:35', 1),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:17:52', 1),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:18:07', 1),
(62, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '16:48:56', 1),
(63, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:28:52', 1),
(64, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:51:52', 1),
(65, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:37:13', 1),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:39:25', 1),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:41:48', 1),
(68, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:44:19', 1),
(69, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:46:49', 1),
(70, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:04:18', 1),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:06:47', 1),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:24:44', 1),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:27:26', 1),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '20:48:31', 1),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '09:10:14', 1),
(76, 'CADASTRO DO CLIENTE ', '', '2016-05-13', '09:40:08', 1),
(77, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '09:41:18', 1),
(78, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '11:28:48', 1),
(79, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '13:03:01', 1),
(80, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '15:22:36', 1),
(81, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '15:54:56', 1),
(82, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '18:02:52', 1),
(83, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '18:32:22', 1),
(84, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '21:40:29', 4),
(85, 'CADASTRO DO CLIENTE ', '', '2016-05-13', '22:03:28', 4),
(86, 'CADASTRO DO CLIENTE ', '', '2016-05-13', '22:03:59', 4),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '22:25:04', 4),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-14', '15:45:43', 4),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '09:59:39', 1),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '11:17:44', 1),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '11:18:25', 1),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '11:22:24', 1),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '21:04:14', 4),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '21:12:39', 4),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '01:58:34', 4),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '02:04:53', 4),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '09:51:04', 1),
(98, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '09:51:31', 1),
(99, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '09:52:45', 1),
(100, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '09:53:05', 1),
(101, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '13:48:59', 1),
(102, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '13:49:56', 1),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '13:50:37', 1),
(104, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '13:50:55', 1),
(105, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-17', '13:52:04', 1),
(106, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '08:26:18', 1),
(107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '08:29:27', 1),
(108, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '08:45:34', 1),
(109, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:29:19', 4),
(110, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:33:09', 4),
(111, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:41:59', 4),
(112, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '13:42:44', 4),
(113, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:30:26', 4),
(114, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:30:58', 4),
(115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:31:29', 4),
(116, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:32:43', 4),
(117, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:33:56', 4),
(118, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:34:52', 4),
(119, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '14:35:41', 4),
(120, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '15:14:28', 0),
(121, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '15:15:03', 0),
(122, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '16:55:09', 1),
(123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-18', '21:59:05', 0),
(124, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'5\'', '2016-05-19', '02:24:58', 1),
(125, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'6\'', '2016-05-19', '02:25:04', 1),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '02:27:14', 1),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '02:52:54', 1),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '13:03:24', 1),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '13:07:25', 1),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-19', '13:08:27', 1),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '16:13:32', 1),
(132, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '17:16:17', 1),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '11:31:19', 1),
(134, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '11:32:44', 1),
(135, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '12:07:04', 1),
(136, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '12:33:20', 1),
(137, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '13:49:19', 1),
(138, 'CADASTRO DO CLIENTE ', '', '2016-05-23', '16:53:35', 2),
(139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:35:39', 2),
(140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:36:23', 2),
(141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:37:07', 2),
(142, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:37:52', 2),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:38:34', 2),
(144, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:39:39', 2),
(145, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:39:56', 2),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:41:48', 2),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:42:04', 2),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:42:45', 2),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:53:35', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:58:42', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:58:47', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:58:50', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '18:59:19', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '22:55:22', 2),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '22:55:45', 2),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '20:27:25', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-23', '21:22:01', 1),
(158, 'DESATIVOU O LOGIN 3', 'UPDATE tb_taxas_entregas SET ativo = \'NAO\' WHERE idtaxaentrega = \'3\'', '2016-05-24', '00:47:58', 2),
(159, 'ATIVOU O LOGIN 3', 'UPDATE tb_taxas_entregas SET ativo = \'SIM\' WHERE idtaxaentrega = \'3\'', '2016-05-24', '00:48:13', 2),
(160, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '00:49:06', 2),
(161, 'CADASTRO DO CLIENTE ', '', '2016-05-24', '00:51:51', 2),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '00:52:25', 2),
(163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '08:52:22', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '09:23:29', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '13:04:40', 2),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '13:11:48', 2),
(167, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '10:44:34', 1),
(168, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:32:59', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:33:28', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:34:23', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:34:51', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:35:36', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:36:51', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '11:45:27', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '15:22:27', 2),
(176, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '15:24:29', 2),
(177, 'CADASTRO DO CLIENTE ', '', '2016-05-24', '12:26:36', 1),
(178, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:16', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:56:09', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '13:00:01', 1),
(181, 'CADASTRO DO CLIENTE ', '', '2016-05-24', '13:00:59', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '16:22:40', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '16:29:09', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '16:29:47', 1),
(185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:08:25', 1),
(186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:16:34', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:17:27', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:18:15', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:18:42', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:19:01', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:39:54', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:42:00', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '17:45:08', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '18:58:26', 1),
(195, 'CADASTRO DO CLIENTE ', '', '2016-05-24', '22:00:31', 2),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '19:12:50', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '19:13:43', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '19:23:28', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '19:24:12', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:13:14', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:14:02', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:14:38', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:15:41', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:16:09', 1),
(205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:16:35', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-05-24', '20:17:49', 1),
(207, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:34:59', 1),
(208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:35:31', 1),
(209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '20:36:03', 1),
(210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-25', '18:50:41', 2),
(211, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'3\'', '2016-05-26', '15:34:43', 0),
(212, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'3\'', '2016-05-26', '15:34:47', 0),
(213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-30', '16:14:31', 0),
(214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-30', '15:50:18', 1),
(215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-30', '16:54:27', 1),
(216, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-30', '21:42:57', 1),
(217, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-30', '22:20:41', 1),
(218, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-30', '22:21:43', 1),
(219, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-31', '09:15:59', 1),
(220, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:35:01', 1),
(221, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:35:53', 1),
(222, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:37:41', 1),
(223, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:38:17', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:43:28', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:45:37', 1),
(226, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:46:55', 1),
(227, 'CADASTRO DO CLIENTE ', '', '2016-05-31', '10:47:37', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:17:55', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:18:13', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:20:25', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:23:36', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:26:44', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:29:14', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:32:27', 1),
(235, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:34:13', 1),
(236, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:35:02', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:35:35', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-06', '19:36:13', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '10:56:06', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:04:12', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:04:35', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:05:44', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:10:27', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:12:48', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:14:17', 1),
(246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:15:04', 1),
(247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:15:50', 1),
(248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:17:29', 1),
(249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:25:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:26:07', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:26:30', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:26:50', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:27:10', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:36:25', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:36:52', 1),
(256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:37:18', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:39:05', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '11:45:56', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '12:06:17', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '12:09:56', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '12:42:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '12:42:39', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '13:07:42', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '13:09:54', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '15:02:23', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-09', '17:12:10', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '08:47:29', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '09:00:49', 1),
(269, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '16:20:41', 1),
(270, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '16:20:56', 1),
(271, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '16:21:07', 1),
(272, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-10', '16:51:08', 1),
(273, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:37:49', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-11', '10:53:35', 1),
(275, 'CADASTRO DO CLIENTE ', '', '2016-06-13', '13:10:18', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-13', '13:10:37', 1),
(277, 'CADASTRO DO CLIENTE ', '', '2016-06-13', '13:11:01', 1),
(278, 'CADASTRO DO CLIENTE ', '', '2016-06-13', '13:11:37', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-13', '13:11:53', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '09:52:06', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '10:49:44', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '11:21:42', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '12:24:31', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '13:16:56', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '15:38:38', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '16:02:53', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-14', '17:47:10', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '08:44:25', 1),
(289, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:20:34', 1),
(290, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:20:45', 1),
(291, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:23:04', 1),
(292, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:23:13', 1),
(293, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:23:28', 1),
(294, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_tipos_veiculos WHERE idtipoveiculo = \'4\'', '2016-06-15', '13:28:11', 1),
(295, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_tipos_veiculos WHERE idtipoveiculo = \'5\'', '2016-06-15', '13:28:18', 1),
(296, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:28:30', 1),
(297, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:28:42', 1),
(298, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:40:52', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '13:41:35', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '13:42:06', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '13:42:29', 1),
(302, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_tipos_veiculos WHERE idtipoveiculo = \'6\'', '2016-06-15', '13:43:36', 1),
(303, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_tipos_veiculos WHERE idtipoveiculo = \'7\'', '2016-06-15', '13:43:43', 1),
(304, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_marcas_veiculos WHERE idmarcaveiculo = \'1\'', '2016-06-15', '13:43:59', 1),
(305, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:44:24', 1),
(306, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:44:36', 1),
(307, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:45:08', 1),
(308, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:45:23', 1),
(309, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:45:44', 1),
(310, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:45:58', 1),
(311, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:46:11', 1),
(312, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:46:22', 1),
(313, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:47:01', 1),
(314, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '13:47:22', 1),
(315, 'DESATIVOU O LOGIN 11', 'UPDATE tb_marcas_veiculos SET ativo = \'NAO\' WHERE idmarcaveiculo = \'11\'', '2016-06-15', '13:51:56', 1),
(316, 'ATIVOU O LOGIN 11', 'UPDATE tb_marcas_veiculos SET ativo = \'SIM\' WHERE idmarcaveiculo = \'11\'', '2016-06-15', '13:52:06', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '10:59:51', 1),
(318, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:51:39', 1),
(319, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:52:15', 1),
(320, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:53:08', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:55:24', 1),
(322, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '11:55:30', 1),
(323, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:55:47', 1),
(324, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:56:01', 1),
(325, 'CADASTRO DO CLIENTE ', '', '2016-06-15', '14:56:44', 1),
(326, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '11:57:20', 1),
(327, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '11:57:55', 1),
(328, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '11:58:32', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '11:59:00', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '11:59:28', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '13:31:48', 1),
(332, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:21:13', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:23:38', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:24:31', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:26:22', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:27:00', 1),
(337, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:27:34', 1),
(338, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '16:28:17', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '17:50:36', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-15', '19:36:49', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-16', '14:49:46', 0),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-16', '14:53:47', 0),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-16', '14:56:45', 0),
(344, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-16', '14:57:40', 0),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-16', '16:45:38', 0),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-16', '16:46:55', 0),
(347, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '13:32:02', 0),
(348, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '13:32:30', 0),
(349, 'CADASTRO DO CLIENTE ', '', '2016-06-17', '13:33:00', 0),
(350, 'CADASTRO DO CLIENTE ', '', '2016-06-18', '13:42:50', 1),
(351, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-20', '16:49:14', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_marcas_veiculos`
--

CREATE TABLE `tb_marcas_veiculos` (
  `idmarcaveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_marcas_veiculos`
--

INSERT INTO `tb_marcas_veiculos` (`idmarcaveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `id_tipoveiculo`) VALUES
(2, 'Agrale', 0, 'SIM', 'agrale', 3),
(3, 'Chevrolet', 0, 'SIM', 'chevrolet', 3),
(4, 'Ford', 0, 'SIM', 'ford', 3),
(5, 'Scania', 0, 'SIM', 'scania', 3),
(6, 'BMW', 0, 'SIM', 'bmw', 1),
(7, 'Chevrolet', 0, 'SIM', 'chevrolet', 1),
(8, 'Fiat', 0, 'SIM', 'fiat', 1),
(9, 'Ford', 0, 'SIM', 'ford', 1),
(10, 'Cross, Trilha e Enduro', 0, 'SIM', 'cross-trilha-e-enduro', 2),
(11, 'Quadriciclos', 0, 'SIM', 'quadriciclos', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_modelos_veiculos`
--

CREATE TABLE `tb_modelos_veiculos` (
  `idmodeloveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_modelos_veiculos`
--

INSERT INTO `tb_modelos_veiculos` (`idmodeloveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `id_marcaveiculo`, `id_tipoveiculo`) VALUES
(1, 'C15', 0, 'SIM', 'c15', 3, 3),
(2, 'Scania 112', 0, 'SIM', 'scania-112', 5, 3),
(3, 'X1', 0, 'SIM', 'x1', 6, 1),
(4, 'Série 1', 0, 'SIM', 'serie-1', 6, 1),
(5, 'Celta', 0, 'SIM', 'celta', 7, 1),
(6, 'Uno', 0, 'SIM', 'uno', 8, 1),
(7, 'Focus Sedan', 0, 'SIM', 'focus-sedan', 9, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anos` int(11) NOT NULL,
  `qtd_vendida` int(11) NOT NULL,
  `importante` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `qtd_visitas` int(11) NOT NULL,
  `garantia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dimensao_externa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_polo` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`, `codigo`, `anos`, `qtd_vendida`, `importante`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `qtd_visitas`, `garantia`, `dimensao_externa`, `tipo_polo`) VALUES
(1, 'Bateria Automotiva MBB', '0906201611121327106685..jpg', '<p>\r\n	01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Produto 01', 'Produto 01', 'Produto 01', 'SIM', 0, 'bateria-automotiva-mbb', 80, '3M', NULL, NULL, '', '', 1, '01-2 - JIS', 1, 2, '<p>\r\n	Para confirma&ccedil;&atilde;o do pedido AP&Oacute;S CONTATO TELEF&Ocirc;NICO favor enviar por e-mail os dados abai xo, anexando comprovante de pagamento (dep&oacute;sito ou transfer&ecirc;ncia banc&aacute;ria) ou especificando o endere&ccedil;o para cobran&ccedil;a (verificar o valor da taxa para cobran&ccedil;a em domic&iacute;lio) para o endere&ccedil;o de e-mail: atendimento@unaflor.com.br</p>\r\n<p>\r\n	Per&iacute;odo da entrega (manh&atilde;, tarde): Obs.: (Caso queira que suas flores sejam entregues em hor&aacute;rio especial, com hora marcada, ser&aacute; co brado uma taxa adicional pelo servi&ccedil;o de R$ 10,00, j&aacute; que uma opera&ccedil;&atilde;o ser&aacute; feita exclusivamente para sua entrega. Escolhendo este servi&ccedil;o, seu pedido ser&aacute; entregue rigorosamente no hor&aacute;rio.)</p>', 1, 6, 3, 2000, 2016, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(2, 'Produto 2 Lorem ipsum dolor sit amet', '0906201611171351894857..jpg', '<p>\r\n	01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'SIM', 0, 'produto-2-lorem-ipsum-dolor-sit-amet', 77, 'Tigre', NULL, NULL, '', '', 3, '02-2 - JIS', 1, 0, '', 1, 9, 7, 1990, 2016, 0, '2 anos', '175 x 195 x 165', '2 polos'),
(3, 'Bateria Carros - Fiat - Uno - 1980 - 2016', '0906201611451372048362..jpg', '<p>\r\n	01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Produto 3', 'Produto 3', 'Produto 3', 'SIM', 0, 'bateria-carros--fiat--uno--1980--2016', 82, 'Amanco', NULL, NULL, '', '', 4, '1', 1, 7, '', 1, 8, 6, 1980, 2016, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(4, 'Produto 4 Lorem ipsum dolor sit amet', '0906201611261266773898..jpg', '<p>01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Produto 4', 'Produto', 'Produto 4', 'SIM', 0, 'produto-4-lorem-ipsum-dolor-sit-amet', 80, 'Sol', NULL, NULL, '', '', 1, '04-2 - JIS', 2, 0, '', 0, 0, 0, 0, 0, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(5, 'Produto 5 Lorem ipsum dolor sit amet', '0906201611261197299503..jpg', '<p>01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-5-lorem-ipsum-dolor-sit-amet', 81, 'Tigre', NULL, NULL, '', '', NULL, '05-2 - JIS', 2, 1, '', 0, 0, 0, 0, 0, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(6, 'Produto 6 Lorem ipsum dolor sit amet', '0906201611261280623160..jpg', '<p>01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-6-lorem-ipsum-dolor-sit-amet', 77, 'Solar', NULL, NULL, '', '', NULL, '06-2 - JIS', 3, 2, '', 0, 0, 0, 0, 0, 10, '2 anos', '175 x 195 x 165', '2 polos'),
(7, 'Produto 7 Lorem ipsum dolor sit amet', '0906201611271296406575..jpg', '<p>01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '', '', 'SIM', 0, 'produto-7-lorem-ipsum-dolor-sit-amet', 77, 'Mais Luz', NULL, NULL, 'https://www.youtube.com/embed/xIM22tZAdwE', '<p>\r\n	Apresenta&ccedil;&atilde;o do v&iacute;deo Lorem Ipsum is simply dummy text of the printing and typesettin g industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a gall ey of type and scrambled it to make a type specimen book. It h as survived not only five centuries, but alsothe leap into electro nic typesetting, remaining essentially unchanged. It was popularis ed in the 1960</p>', NULL, '07-2 - JIS', 3, 1, '', 0, 0, 0, 0, 0, 0, '3 anos', '175 x 195 x 165', '2 polos'),
(8, 'Produto 8 Lorem ipsum dolor sit amet', '0906201611391142641447..jpg', '<p>\r\n	08 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '', '', 'SIM', 0, 'produto-8-lorem-ipsum-dolor-sit-amet', 75, 'Tigre', NULL, NULL, '', '', NULL, '08-2 - JIS', 3, 2, '', 0, 0, 0, 0, 0, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(9, 'Produto 9 Lorem ipsum dolor sit amet', '0906201611361213502592..jpg', '<p>\r\n	08 &nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', '', '', 'SIM', 0, 'produto-9-lorem-ipsum-dolor-sit-amet', 74, 'Amanco', NULL, NULL, '', '', NULL, '09-2 - JIS', 3, 12, '', 0, 0, 0, 0, 0, 0, '2 anos', '175 x 195 x 165', '2 polos'),
(10, 'Produto 1001 Lorem ipsum dolor sit amet', '0906201611371327772690..jpg', '<p>\r\n	01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '10', '', '', 'SIM', 0, 'produto-1001-lorem-ipsum-dolor-sit-amet', 82, 'Lual', NULL, NULL, '', '', 4, '1001-2 - JIS', 3, 10, '', 0, 0, 0, 0, 0, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(11, 'Bateria BMW X1, celta, Uno', '1706201601241397409859..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'SIM', 0, 'bateria-bmw-x1-celta-uno', 0, NULL, NULL, NULL, NULL, NULL, NULL, '3342', 0, 0, '', 1, 0, 0, 1990, 2016, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(15, 'Bateria para Celta e Uno', '1706201601241397409859..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'bateria-para-celta-e-uno', 3, NULL, NULL, NULL, NULL, NULL, NULL, '6544', 0, 0, '', 1, 0, 0, 2000, 2016, 0, '1 ano', '175 x 195 x 165', '2 polos'),
(16, 'Bateria Estacionária Freedom DF300 12V 30Ah', '1706201601511344200346.jpeg', '<p>\r\n	A Bateria Estacion&aacute;ria Freedom DF300, possui capacidade de 30Ah para descarga em 100h (C100) e 26Ah em 20h a (C20) A linha de baterias estacion&aacute;rias Freedom possuem vida &uacute;til projetada superior a 4 anos e garantia de 2 anos contra defeitos de fabrica&ccedil;&atilde;o. N&atilde;o necessita de nenhuma manuten&ccedil;&atilde;o nem reposi&ccedil;&atilde;o de &aacute;gua.</p>\r\n<p>\r\n	Tampa selada* evitando escape de eletr&oacute;lito para o exterior<br />\r\n	Filtro anti-chamas* que permite o escape de gases e impede a penetra&ccedil;&atilde;o de chamas para o interior da bateria<br />\r\n	Grade com liga de Chumbo-C&aacute;lcio-Prata faz com que a bateria Freedom seja a de menor perda de &aacute;gua no mercado e de melhor durabilidade<br />\r\n	Design da grade radial e refor&ccedil;ado, que permite melhor condu&ccedil;&atilde;o de corrente e melhor durabilidade da bateria em servi&ccedil;o<br />\r\n	Eletr&oacute;lito Fluido permite melhor dissipa&ccedil;&atilde;o t&eacute;rmica da bateria, conferindo vantagens em rela&ccedil;&atilde;o a baterias VRLA que s&atilde;o mais sens&iacute;veis a varia&ccedil;&atilde;o de temperatura<br />\r\n	Solda Intercelular com &aacute;rea de solda 36% maior que baterias do mercado e permite melhor condu&ccedil;&atilde;o de corrente e melhor efici&ecirc;ncia el&eacute;trica da bateria em processo de carga e descarga<br />\r\n	Placas espessas de alta densidade<br />\r\n	Separadores de polietileno, em forma de envelope, com alta resist&ecirc;ncia mec&acirc;nica<br />\r\n	Caixa e tampa de polipropileno de alta resist&ecirc;ncia a impactos, com tampas seladas por fus&atilde;o do material, sem possibilidade de apresentar vazamentos<br />\r\n	Indicador de teste* que permite imediata visualiza&ccedil;&atilde;o das condi&ccedil;&otilde;es da bateria para teste, orientando seu diagn&oacute;stico:<br />\r\n	Tipos de terminais:<br />\r\n	DF300, DF500, DF700, DF1000, DF2500, DF3000 e DF4001: Terminal &ldquo;L&rdquo;, de chumbo<br />\r\n	DF2000: rosqueados, de a&ccedil;o inoxid&aacute;vel (rosca de 3/8&rdquo; 16unc)<br />\r\n	*N&atilde;o se aplica a bateria DF4001</p>', '', '', '', 'SIM', 0, 'bateria-estacionaria-freedom-df300-12v-30ah', 1, NULL, NULL, NULL, NULL, NULL, NULL, '92876-08', 0, 0, '', 1, 0, 0, 1987, 2013, 0, '1 Ano', '175 x 195 x 165', '2 polos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_categorias`
--

CREATE TABLE `tb_produtos_categorias` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_categoriaproduto` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos_categorias`
--

INSERT INTO `tb_produtos_categorias` (`id`, `id_produto`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(14, 1, 75, 'SIM', 0, ''),
(15, 1, 76, 'SIM', 0, ''),
(16, 1, 77, 'SIM', 0, ''),
(17, 82, 81, 'SIM', 0, ''),
(18, 82, 78, 'SIM', 0, ''),
(19, 83, 75, 'SIM', 0, ''),
(20, 83, 80, 'SIM', 0, ''),
(21, 84, 74, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(3, 16, 1, 6, 4, 1987, 2013, 'SIM', 0, ''),
(4, 16, 1, 7, 5, 1987, 2013, 'SIM', 0, ''),
(5, 16, 1, 8, 6, 1987, 2013, 'SIM', 0, ''),
(6, 16, 1, 9, 7, 1987, 2013, 'SIM', 0, ''),
(7, 15, 1, 7, 5, 2000, 2016, 'SIM', 0, ''),
(8, 15, 1, 8, 6, 2000, 2016, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_promocoes`
--

CREATE TABLE `tb_promocoes` (
  `idpromocao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_promocoes`
--

INSERT INTO `tb_promocoes` (`idpromocao`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `url`) VALUES
(44, 'promocao 02', NULL, '2405201601001294828233..jpg', 'SIM', NULL, 'promocao-02', '', '', '', NULL, '/produtos'),
(43, 'promoção produto 3', '<div>\r\n	teste 01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div>\r\n<div>\r\n	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div>\r\n<div>\r\n	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div>\r\n<div>\r\n	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div>\r\n<div>\r\n	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div>\r\n<div>\r\n	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', '2405201601001321742149..jpg', 'SIM', NULL, 'promocao-produto-3', '', '', '', NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index', NULL, NULL, 'SIM', NULL, NULL),
(2, 'Empresa', 'Empresa', NULL, NULL, 'SIM', NULL, NULL),
(3, 'Orçamentos', 'Orçamentos', NULL, NULL, 'SIM', NULL, NULL),
(4, 'Produtos', 'Produtos', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Contatos', 'Contatos', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Trabalhe conosco', 'Trabalhe conosco', NULL, NULL, 'SIM', NULL, NULL),
(8, 'Serviços', 'Serviços', NULL, NULL, 'SIM', NULL, NULL),
(10, 'Dicas', 'Dicas', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`) VALUES
(40, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', '<p>\r\n	teste 1 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1506201611551181342744..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-1', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', 0, '', '', '2104201610131175693451.png'),
(41, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', '<p>\r\n	teste 2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1506201611571262485918..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-2', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', 0, '', '', '2104201610131348432865.png'),
(42, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', '<p>\r\n	teste 3 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1506201611571254065056..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-3', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', 0, '', '', '2104201610131240709875.png'),
(43, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE  DO SERVIÇO GRANDE PARA TESTE 4', '<p>\r\n	teste 4 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1506201611581348847654..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste--do-servico-grande-para-teste-4', '', '', '', 0, '', '', '2104201610141153328479.png'),
(44, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 5', '<p>\r\n	teste 5 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1506201611581395787347..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-5', '', '', '', 0, '', '', '2104201610431210084250.png'),
(45, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 6', '<p>\r\n	teste 6 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1506201611591206368124..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-6', '', '', '', 0, '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sugestoes_mensagens`
--

CREATE TABLE `tb_sugestoes_mensagens` (
  `idsugestaomensagem` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  `id_categoriamensagem` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_sugestoes_mensagens`
--

INSERT INTO `tb_sugestoes_mensagens` (`idsugestaomensagem`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `id_categoriamensagem`) VALUES
(1, 'Podem existir mil obstáculos', '<p>\r\n	Podem existir mil obst&aacute;culos, mas nada far&aacute; com que meu amor por ti morra. Atravessarei at&eacute; os maiores mares, mas n&atilde;o existir&aacute; &aacute;gua suficiente que afogue o amor que sinto por voc&ecirc;.</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'podem-existir-mil-obstaculos', 'Podem existir mil obstáculos', '', '', NULL, 4),
(2, 'Subirei até a montanha', 'Subirei até a montanha mais alta do mundo, só para te ver, e de lá gritarei seu nome para ver se me ouve, e se me ouvires, direi uma só frase: Eu te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(3, 'E quando o vento passar', 'E quando o vento passar, levará consigo o que eu disse, e quando ele soprar em seu ouvido, escutarás junto ao vento: Eu te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(4, 'E toda vez que o vento soprar', 'E toda vez que o vento soprar em seu ouvido, não será só apenas o vento, mas eu dizendo que te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(5, 'Desejo a você', '<p>\r\n	Um anivers&aacute;rio cheio de paz...<br />\r\n	Que os sentimentos mais puros<br />\r\n	se concretizem em gestos de bondade,<br />\r\n	e o amor encontre abertas as portas<br />\r\n	do seu cora&ccedil;&atilde;o.<br />\r\n	Que voc&ecirc; possa guardar deste anivers&aacute;rio<br />\r\n	as melhores lembran&ccedil;as.<br />\r\n	E que tudo contribua para sua felicidade.</p>\r\n<p>\r\n	Abra&ccedil;os e Feliz Anivers&aacute;rio!</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'desejo-a-voce', '', '', '', NULL, 1),
(6, 'Seja Muito Feliz, Hoje e Sempre', 'Feliz aniversário! Que hoje todos os sorrisos se abram para você, que seu coração seja inundado pela alegria e gratidão pela vida e que as homenagens abundem, pois você merece viver um dia muito especial.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 'Duradouro e feliz', '<p>\r\n	A vida &eacute; feita de momentos bons e ruins. Aproveitem intensamente os bons e estejam unidos para superar os ruins. Assim se constr&oacute;i um casamento duradouro e feliz!</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'duradouro-e-feliz', '', '', '', NULL, 2),
(8, 'O amor de um homem', '<p>\r\n	O amor de um homem por uma mulher &eacute; muito mais profundo e verdadeiro quando Deus acontece primeiro na vida do casal.</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'o-amor-de-um-homem', 'O amor de um homem', 'O amor de um homem', 'O amor de um homem', NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_veiculos`
--

CREATE TABLE `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`) VALUES
(1, 'Carros', 0, 'SIM', 'carros'),
(2, 'Motos', 0, 'SIM', 'motos'),
(3, 'Caminhões', 0, 'SIM', 'caminhoes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuarios`
--

CREATE TABLE `tb_usuarios` (
  `idusuario` int(10) UNSIGNED NOT NULL,
  `nome` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `rg` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `tel_celular` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tel_residencial` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`idusuario`, `nome`, `email`, `senha`, `cpf`, `rg`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `tel_celular`, `tel_residencial`, `ativo`, `ordem`, `url_amigavel`) VALUES
(7, 'Marcio André da Silva', 'marcio@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '1905364', 'Quadra 204 Conjunto 17 Lote', '08', 'Casa 2', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8606-6484', '(61) 3456-7864', 'SIM', 0, 'marcio-andre-da-silva'),
(8, 'Marcio', 'marciomas@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '8272916', 'Quadra 204 conjunto 20 lote', '1', '', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8539-8898', '(61) 3334-0987', 'SIM', 0, 'marcio'),
(9, 'MOBILE', '', 'cb7e7bca423a4b5a591a2c5bf49c688b', '697834785', '7548675', 'MOBILE', '8', 'MOBILE', 'MOBILE', 'MOBILE', 'MO', '9765341867', '4581236714', 'SIM', 0, 'mobile'),
(10, 'Joana Dark', 'joana@masmidia.com.br', 'd41d8cd98f00b204e9800998ecf8427e', '917.639.161-20', '1905361', 'Quadra 204 conjunto 17 lote', '8', 'casa 2', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8604-9092', '(61) 3847-4939', 'SIM', 0, 'joana-dark'),
(11, 'Fabiana', 'fabiana@masmidia.com.br', 'd41d8cd98f00b204e9800998ecf8427e', '917.639.161-20', '1896788', 'Quadra 200 lote', '8', 'apt 101', 'recanto das emas', 'brasilia', 'DF', '(61) 9837-8787', '(61) 8308-9221', 'SIM', 0, 'fabiana'),
(12, 'fabio', 'fabio@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '9128937981', 'quadra 200 lote', '4', 'apt 5', 'recanto das emas', 'brasíliad', 'DF', '(61) 9373-8287', '(51) 8189-3637', 'SIM', 0, 'fabio'),
(13, 'mobile', 'mobile@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '576576576', '576576', 'quadra 204', '8', 'casa 1', 'recanto das emas', 'brasília', 'df', '7861872358761', '675126785673', 'SIM', 0, 'mobile'),
(14, 'User14', 'user14@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 200 conjunto 20', '5', 'casa 1', 'Recanto das Emas', 'Brasília', 'DF', '8765-0987', '3332-0987', 'SIM', 0, 'user14'),
(15, 'Homeweb', 'homewebbrasil@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 100', '8', 'casa 3', 'asa sul', 'brasília', 'df', '8765-0989', '3437-9876', 'SIM', 0, 'homeweb'),
(16, 'Terezinha Aparecida', 'terezinha@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 200 lote', '10', 'casa 5', 'Recanto das Emas', 'Brasília', 'DF', '91069282', '3423-0987', 'SIM', 0, 'terezinha-aparecida'),
(17, 'Miguel José', 'miguel@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 106 conjunto 20 lote', '10', 'apt 202', 'Recanto das Emas', 'Brasília', 'DF', '8876-0988', '3561-0987', 'SIM', 0, 'miguel-jose'),
(18, 'JOANA SILVA', 'joanasilva@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'QUADRA 100 LOTE', '34', 'CASA 2', 'RECANTO DAS EMAS', 'BRASÍLIA', 'DF', '8765-0998', '3456-0098', 'SIM', 0, 'joana-silva'),
(19, 'Bianca Barros', 'biancabarros1@hotmail.com', '729987adc7a9936a7ae9ba744291e35d', '', '', 'RUA SACRAMENTO', '0', 'QD 35, LT 20', 'CARDOSO', 'APARECIDA DE  GOIANIA', '74', '96871100', '62 35186845', 'SIM', 0, 'bianca-barros'),
(20, 'HOMEWEB TESTE', 'atendimento.sites@homewebbrasil.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'ENDEREÇO DO CONTATO', 'TESTE', 'COMPLEMENTO', 'TESTE', 'TESTE', 'GO', '(22) 2222-2222', '(22) 2222-2222', 'SIM', 0, 'homeweb-teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vendas`
--

CREATE TABLE `tb_vendas` (
  `idvenda` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `mensagem_cartao` longtext COLLATE utf8_unicode_ci,
  `id_frete` int(11) DEFAULT NULL,
  `endereco_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complemento_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_venda` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'AGUARDANDO PAGAMENTO',
  `ponto_referencia` longtext CHARACTER SET utf8,
  `observacoes` longtext COLLATE utf8_unicode_ci,
  `horario_entrega` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg_email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `msg_deposito` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_pagamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_frete` double NOT NULL,
  `total` double NOT NULL,
  `data_pagamento` date NOT NULL,
  `data_separacao_entrega` date NOT NULL,
  `data_entrega` date NOT NULL,
  `data_entrega_cliente` date NOT NULL,
  `celular_contato` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vendas_produtos`
--

CREATE TABLE `tb_vendas_produtos` (
  `id_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `valor` double DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_vendas_produtos`
--

INSERT INTO `tb_vendas_produtos` (`id_venda`, `id_produto`, `valor`, `qtd`, `titulo`, `ativo`, `ordem`) VALUES
(1, 13, 180, 2, 'Produto 9 Lorem ipsum dolor sit amet', 'SIM', NULL),
(1, 10, 222.98, 1, 'Produto 6 Lorem ipsum dolor sit amet', 'SIM', NULL),
(4, 11, 120.93, 1, 'Produto 7 Lorem ipsum dolor sit amet', 'SIM', NULL),
(5, 12, 230, 1, 'Produto 8 Lorem ipsum dolor sit amet', 'SIM', NULL),
(10, 1, 45, 1, 'Produto 1 Lorem ipsum dolor sit amet', 'SIM', NULL),
(11, 10, 222.98, 1, 'Produto 6 Lorem ipsum dolor sit amet', 'SIM', NULL),
(12, 12, 230, 1, 'Produto 8 Lorem ipsum dolor sit amet', 'SIM', NULL),
(13, 14, 90, 1, 'Produto 1001 Lorem ipsum dolor sit amet', 'SIM', NULL),
(14, 13, 180, 1, 'Produto 9 Lorem ipsum dolor sit amet', 'SIM', NULL),
(15, 9, 112.98, 1, 'Produto 5 Lorem ipsum dolor sit amet', 'SIM', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_mensagens`
--
ALTER TABLE `tb_categorias_mensagens`
  ADD PRIMARY KEY (`idcategoriamensagem`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  ADD PRIMARY KEY (`idequipamento`);

--
-- Indexes for table `tb_fretes`
--
ALTER TABLE `tb_fretes`
  ADD PRIMARY KEY (`idfrete`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_marcas_veiculos`
--
ALTER TABLE `tb_marcas_veiculos`
  ADD PRIMARY KEY (`idmarcaveiculo`);

--
-- Indexes for table `tb_modelos_veiculos`
--
ALTER TABLE `tb_modelos_veiculos`
  ADD PRIMARY KEY (`idmodeloveiculo`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_produtos_categorias`
--
ALTER TABLE `tb_produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  ADD PRIMARY KEY (`idprodutosmodeloaceito`);

--
-- Indexes for table `tb_promocoes`
--
ALTER TABLE `tb_promocoes`
  ADD PRIMARY KEY (`idpromocao`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Indexes for table `tb_sugestoes_mensagens`
--
ALTER TABLE `tb_sugestoes_mensagens`
  ADD PRIMARY KEY (`idsugestaomensagem`);

--
-- Indexes for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  ADD PRIMARY KEY (`idtipoveiculo`);

--
-- Indexes for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indexes for table `tb_vendas`
--
ALTER TABLE `tb_vendas`
  ADD PRIMARY KEY (`idvenda`);

--
-- Indexes for table `tb_vendas_produtos`
--
ALTER TABLE `tb_vendas_produtos`
  ADD PRIMARY KEY (`id_venda`,`id_produto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_categorias_mensagens`
--
ALTER TABLE `tb_categorias_mensagens`
  MODIFY `idcategoriamensagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  MODIFY `idequipamento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_fretes`
--
ALTER TABLE `tb_fretes`
  MODIFY `idfrete` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;
--
-- AUTO_INCREMENT for table `tb_marcas_veiculos`
--
ALTER TABLE `tb_marcas_veiculos`
  MODIFY `idmarcaveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_modelos_veiculos`
--
ALTER TABLE `tb_modelos_veiculos`
  MODIFY `idmodeloveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_produtos_categorias`
--
ALTER TABLE `tb_produtos_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  MODIFY `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_promocoes`
--
ALTER TABLE `tb_promocoes`
  MODIFY `idpromocao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tb_sugestoes_mensagens`
--
ALTER TABLE `tb_sugestoes_mensagens`
  MODIFY `idsugestaomensagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  MODIFY `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `idusuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_vendas`
--
ALTER TABLE `tb_vendas`
  MODIFY `idvenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_vendas_produtos`
--
ALTER TABLE `tb_vendas_produtos`
  MODIFY `id_venda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php /* ?>
<div class=" col-xs-4 subir_contatos_home">
<!--  ==============================================================  -->
<!--CONTATOS BANNER -->
<!--  ==============================================================  -->
<div class="col-xs-9 text-right padding0 contatos_home">
  <div class="media">
    <div class="media-left media-middle">
  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone-azul.png" alt="">
  </div>
  <div class="media-body">
  <h3 class="media-heading"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h3>
  </div>
</div>

</div>
<!--  ==============================================================  -->
<!--CONTATOS BANNER -->
<!--  ==============================================================  -->

<!--  ==============================================================  -->
<!-- MAIS CONTATOS -->
<!--  ==============================================================  -->
<div class="col-xs-3 contatos_home padding0">

<a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn_maiscontatos">
<img src="<?php echo Util::caminho_projeto() ?>/imgs/botao_outros_telefones.png" alt="">
</a>


<ul class="dropdown-menu contatos-topo">
<li><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></li>

<?php if (!empty($config[telefone2])): ?>
<li><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></li>
<?php endif ?>

<?php if (!empty($config[telefone3])): ?>
<li><?php Util::imprime($config[ddd3]); ?></span> <?php Util::imprime($config[telefone3]); ?></li>
<?php endif ?>

<?php if (!empty($config[telefone4])): ?>
<li><?php Util::imprime($config[ddd4]); ?></span> <?php Util::imprime($config[telefone4]); ?></li>
<?php endif ?>

</ul>
</div>
<!--  ==============================================================  -->
<!-- MAIS CONTATOS -->
<!--  ==============================================================  -->

</div>
<?php */ ?>




<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="<?php echo Util::caminho_projeto(); ?>/jquery/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->







<!-- Custom styles for this template -->
<link href="<?php echo Util::caminho_projeto() ?>/dist/css/non-responsive.css" rel="stylesheet">


<script src="<?php echo Util::caminho_projeto(); ?>/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>





<!--    ==============================================================  -->
<!--    ROYAL SLIDER    -->
<!--    ==============================================================  -->

<!-- slider JS files -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>






<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/ie10-viewport-bug-workaround.js"></script>






<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/bootstrapValidator.min.css"/>
 <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/font-awesome.min.css"/>


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrap-lightbox.min.js"></script>



<!-- The paths might be changed to suit with your folder structure -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/intlTelInput.css" />
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/intlTelInput.min.js"></script>






<?php /*

<!-- Rating
http://plugins.krajee.com/star-rating
================================================== -->
<link href="<?php echo Util::caminho_projeto() ?>/dist//css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo Util::caminho_projeto() ?>/dist//js/star-rating.min.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function () {

    $('.avaliacao').rating({
          min: 0,
          max: 5,
          step: 1,
          size: 'xs',
          showClear: false,
          disabled: true,
          clearCaption: 'Seja o primeiro a avaliar.',
          starCaptions: {
                            0.5: 'Half Star',
                            1: 'Ruim',
                            1.5: 'One & Half Star',
                            2: 'Regular',
                            2.5: 'Two & Half Stars',
                            3: 'Bom',
                            3.5: 'Three & Half Stars',
                            4: 'Ótimo',
                            4.5: 'Four & Half Stars',
                            5: 'Excelente'
                        }
       });


});
</script>

*/ ?>




<script>
    $('#myAffix').affix({
      offset: {
        top: 100,
        bottom: function () {
          return (this.bottom = $('.footer').outerHeight(true))
        }
      }
    })
</script>









<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
    window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>





<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />










<!-- ---- LAYER SLIDER ---- -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin.css" />
<script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>





<!--    ====================================================================================================     -->
<!--    CLASSE DE FONTS DO SITE http://fortawesome.github.io/Font-Awesome/icons/    -->
<!--    ====================================================================================================     -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">










<link href="<?php echo Util::caminho_projeto() ?>/dist/css/icons.css" rel="stylesheet">





<!-- usando carroucel 01 -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel-categorias').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 160,
        itemMargin: 1,
        asNavFor: '#slider'
      });

      $('#slider-categorias').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 140,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>





<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->
<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/bootstrap-select/dist/css/bootstrap-select.min.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/bootstrap-select/dist/js/bootstrap-select.min.js"></script>


  <script type="text/javascript">
    $('.selectpicker').selectpicker({
      style: 'btn-info',
      size: 4
    });

  </script>
<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- DIRECIONA SELECT    -->
<!-- ======================================================================= -->
<script>
  $(function(){
      // bind change event to select
      $('.submit-select').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
<!-- ======================================================================= -->
<!-- DIRECIONA SELECT    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!--PRODUTO HOVER MOSTRANDO CARRINHO E SAIBA MAIS    -->
<!-- ======================================================================= -->

<script type="text/javascript">
$(document).ready(function() {

  $(".produto-hover").hover(
    function () {
      $(this).stop().animate({opacity:1});
    },
    function () {
      $(this).stop().animate({opacity:0});
    }
    );

});
</script>
<!-- ======================================================================= -->
<!--PRODUTO HOVER MOSTRANDO CARRINHO E SAIBA MAIS    -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- SCROLL-->
<!--  ==============================================================  -->
<script type = "text/javascript">
      $(function () {
          $('#scrollToBottom').bind("click", function () {
              $('html, body').animate({ scrollTop: $(document).height() }, 4000);
              return false;
          });
          // $('#scrollToTop').bind("click", function () {
          //     $('html, body').animate({ scrollTop: 0 }, 1200);
          //     return false;
          // });
      });
  </script>
  <!--  ==============================================================  -->
  <!-- SCROLL-->
  <!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- lightbox -->
<!--  ==============================================================  -->
<script src="<?php echo Util::caminho_projeto() ?>/jquerylightbox2-master/dist/js/lightbox-plus-jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquerylightbox2-master/dist/css/lightbox.min.css">
<!--  ==============================================================  -->
<!-- lightbox -->
<!--  ==============================================================  -->





<script type="text/javascript">
  $(document).ready(function(){
      $('#topo_tipos_veiculos').change(function(){
          $('#topo_marca_veiculos').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_marcas_veiculos_url_amigavel.php?id='+$('#topo_tipos_veiculos').val() );
      });
  });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#topo_marca_veiculos').change(function(){
            $('#topo_modelo_veiculos').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_modelos_veiculos_url_amigavel.php?id='+$('#topo_marca_veiculos').val()+'&tipo_veiculo='+$('#topo_tipos_veiculos').val() );
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#topo_modelo_veiculos').change(function(){
            $('#busca_topo_ano').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_ano.php');
        });
    });
</script>











<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?499ueKvNHiNtbLnMfCnkHYg51zLtJ0TS";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

<?php
if(mysql_num_rows($result) == 0){
  echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhum produto encontrado.</p>";
}else{
  $i = 0;
  while($row = mysql_fetch_array($result)){
  ?>
    <div class="col-xs-4 pg10">
      <div class="thumbnail produtos-home">

       <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 271, 218, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

       <div class="produto-hover">
         <div class="col-xs-6 text-center">
           <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba mais">
             <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.png" alt="" class="input100">
           </a>
         </div>
         <div class="col-xs-6 hover-btn-orcamento">
           <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
             <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.png" alt="" class="input100">
           </a>
         </div>

       </div>

       <!--  ==============================================================  -->
       <!-- TEXT DIAGONAL-->
       <!--  ==============================================================  -->
       <div class="diagonal">
         <h3><?php Util::imprime($row[garantia]); ?></h3>
       </div>
       <!--  ==============================================================  -->
       <!-- TEXT DIAGONAL-->
       <!--  ==============================================================  -->

       <div class="col-xs-12 padding0">
       <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_star_produto.png" alt="" />
       </div>

       <div class="caption top10">
         <h1><?php Util::imprime($row[titulo]); ?></h1>
           <div class="top10">
           <p><?php Util::imprime($row[descricao], 400); ?></p>
         </div>
         <div class="top15">
             <h2>Ampere: <?php Util::imprime($row[codigo]); ?></h2>
         </div>
       </div>
     </div>

   </div>

   <?php
      if ($i == 2) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }
    }
  }
  ?>

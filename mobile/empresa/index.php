<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
    <!doctype html>
    <html>

    <head>
        <?php require_once('.././includes/head.php'); ?>



    </head>

    <!--  ==============================================================  -->
    <!-- background -->
    <!--  ==============================================================  -->
    <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
        <style>
            .bg-interna {
                background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 247px center no-repeat;
            }

        </style>

        <body class="bg-interna">


            <!-- ======================================================================= -->
            <!-- topo    -->
            <!-- ======================================================================= -->
            <?php require_once('../includes/topo.php') ?>
                <!-- ======================================================================= -->
                <!-- topo    -->
                <!-- ======================================================================= -->



                <div class="container">
                    <div class="row top5">
                        <div class="col-xs-12">
                            <!-- ======================================================================= -->
                            <!-- Breadcrumbs    -->
                            <!-- ======================================================================= -->
                            <div class="breadcrumb">
                                <a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
                                <a class="active">Empresa</a>
                            </div>
                            <!-- ======================================================================= -->
                            <!-- Breadcrumbs    -->
                            <!-- ======================================================================= -->
                        </div>
                    </div>
                </div>


                <div class="container">
                    <div class="row top35">
                        <div class="col-xs-9 empresa_descricao">
                            <h1><span>CONHEÇA O </span>SHOPPING DAS BATERIAS</h1>
                        </div>

                        <div class="clearfix"></div>

                        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                            <div class="col-xs-12 top105 empresa_descricao_geral">
                                <p>
                                    <?php Util::imprime($row[descricao]); ?>
                                </p>
                                <div class="col-xs-12  top50">
                                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_chegar_mais.png" alt="">
                                </div>

                                <div class="col-xs-12">
                                    <div class="top40 bottom20">
                                        <p>
                                            <?php Util::imprime($config[endereco]); ?>
                                        </p>
                                    </div>
                                    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="489" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>

                            </div>


                            <div class="clearfix"> </div>


                            <div class="top70"></div>
                            <!--  ==============================================================  -->
                            <!--NOSSAS MARCAS-->
                            <!--  ==============================================================  -->
                            <?php require_once("../includes/marcas.php"); ?>
                            <!--  ==============================================================  -->
                            <!--NOSSAS MARCAS-->
                            <!--  ==============================================================  -->


                    </div>
                </div>









                <?php require_once('../includes/rodape.php'); ?>

        </body>

    </html>
<?php require_once('../includes/js_css.php'); ?>

<div class="container">
  <div class="row topo top10">


    <!--  ==============================================================  -->
    <!-- BARRA PESQUISA -->
    <!--  ==============================================================  -->
      <div class="col-xs-6 pesquisa">


           <!-- Button trigger modal -->
            <button type="button" class="btn btn_pesquisa_avancada input100" data-toggle="modal" data-target="#myModal">
               <i class="fa fa-search" aria-hidden="true"></i>
               BUSCA AVANÇADA
            </button>

            <!-- Modal -->
            <form action="" method="post" onsubmit="this.action = '<?php echo Util::caminho_projeto() ?>/mobile/produtos/'+this.topo_tipos_veiculos.value+'/'+this.topo_marca_veiculos.value+'/'+this.topo_modelo_veiculos.value+'/'+this.busca_topo_ano.value " >
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            BUSCA AVANÇADA
                        </h4>
                      </div>
                      <div class="modal-body">
                               <select name="modelos" id="topo_tipos_veiculos" class="input100 select_tipos_veiculos">
                                    <option value="<?php echo Util::caminho_projeto(); ?>/mobile/produtos/">SELECIONE O TIPO DE VEÍCULO</option>
                                    <?php
                                    $result = $obj_site->select("tb_tipos_veiculos", "and idtipoveiculo IN(1,2)");
                                    if (mysql_num_rows($result) > 0) {
                                        while ($row = mysql_fetch_array($result)) {
                                        ?>
                                            <option value="<?php Util::imprime($row[url_amigavel]); ?>"><?php Util::imprime($row[titulo]) ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>

                                <select name="topo_marca_veiculos" id="topo_marca_veiculos" style="display: none;" class="input100 select_tipos_veiculos top25">
                                    <option value="">MARCA</option>
                                </select>

                                <select name="topo_modelo_veiculos" id="topo_modelo_veiculos" style="display: none;" class="input100 select_tipos_veiculos top25">
                                    <option value="">MODELO</option>
                                </select>

                                <select name="busca_topo_ano" id="busca_topo_ano" style="display: none;" class="input100 select_tipos_veiculos top25">
                                    <option value="">ANO</option>
                                </select>

                      </div>
                      <div class="modal-footer">
                        <input type="submit" id="btn_envia_cat_prod" value="ENVIAR" style="display: none;" class="select_tipos_veiculos col-xs-3 pull-right top25">
                      </div>
                    </div>
                  </div>
                </div>
            </form>



      </div>
      <!--  ==============================================================  -->
      <!-- BARRA PESQUISA -->
      <!--  ==============================================================  -->




      <!-- ======================================================================= -->
      <!-- botao carrinho de compra -->
      <!-- ======================================================================= -->
      <div class="col-xs-6 dropdown carrinho_topo">

        <button class="btn btn_carrinho input100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="badge"><?php echo count($_SESSION[solicitacoes_produtos]); ?></span>
        </button>

        <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

         <?php
        if(count($_SESSION[solicitacoes_produtos]) > 0)
        {
            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
            {
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
            <div class="lista-itens-carrinho">
              <div class="col-xs-3">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
              </div>
              <div class="col-xs-9">
                <h1><?php Util::imprime($row[titulo]) ?></h1>
              </div>

            </div>
            <?php
          }
        }


        ?>

        <div class="text-right top10 bottom10 col-xs-12">
          <a class="btn btn-finaliza-orcamento" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="Finalizar" >
            FINALIZAR
          </a>
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- botao carrinho de compra -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- LOGO -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 top15 text-center">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="HOME">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
          </a>
    </div>
    <!-- ======================================================================= -->
    <!-- LOGO -->
    <!-- ======================================================================= -->

  </div>
</div>



<!-- ======================================================================= -->
<!-- MENU  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top15">
    <div class="bg-menu">
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
        <div class="col-xs-4 padding0">
          <select name="menu" id="menu-site" class="select-menu pg10">
            <option value=""></option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile">HOME</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/contatos">CONTATOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</option>

          </select>
        </div>
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- CONTATOS  -->
        <!-- ======================================================================= -->
        <div class="col-xs-8 text-right">

          <div class="media pt15">

            <div class="media-body">
              <h4 class="right5">
                <span><?php Util::imprime($config[ddd1]); ?></span>
                <?php Util::imprime($config[telefone1]); ?>
              </h4>
            </div>

            <div class="media-left media-middle">
              <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_contatos.png" alt="" />
              </a>
            </div>

          </div>

        </div>
        <!-- ======================================================================= -->
        <!-- CONTATOS  -->
        <!-- ======================================================================= -->



    </div>
  </div>
</div>

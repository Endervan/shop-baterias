
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- <script type="text/javascript">
    $(document).ready(function() {
      $('#Carousel').carousel({
        interval: 5000
      })
    });
  </script> -->



</head>

<body>

      <!-- ======================================================================= -->
      <!-- topo    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/topo.php') ?>
      <!-- ======================================================================= -->
      <!-- topo    -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- BG HOME    -->
      <!-- ======================================================================= -->
      <div class="container">
        <div class="row">
          <!--  ==============================================================  -->
          <!--  CARROUCEL PRODUTOS-->
          <!--  ==============================================================  -->
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators navs_produtos_home">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 2 order by rand() limit 3 ");
                if(mysql_num_rows($result) > 0){
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                    <?php
                    $i++;
                  }
                }
                ?>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <?php
                if (count($imagens) > 0) {
                  $i = 0;
                  foreach ($imagens as $key => $imagem) {
                    ?>
                    <div class="item <?php if($i == 0){ echo "active"; } ?>">

                      <?php if (!empty($imagem[url])): ?>
                        <a href="<?php Util::imprime($imagem[url]); ?>" >
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                        </a>
                      <?php else: ?>
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                      <?php endif; ?>

                    </div>
                    <?php
                    $i++;
                  }
                }
                ?>
              </div>

              <!-- Controls -->
              <a class="left carousel-control controles_produtos_home" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control controles_produtos_home" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <!--  ==============================================================  -->
            <!--  CARROUCEL PRODUTOS-->
            <!--  ==============================================================  -->
        </div>
      </div>
      <!-- ======================================================================= -->
      <!-- BG HOME    -->
      <!-- ======================================================================= -->






  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- MENU CATEGORIAS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 text-center top25 bottom25">


          <?php
              $i = 1;
              $result = $obj_site->select("tb_tipos_veiculos", "order by rand() limit 6");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  //  busca a qtd de produtos cadastrados
                  $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


                  switch ($i) {
                    case 1:
                       $cor_class = 'bateria_caminhao';
                    break;
                    case 2:
                       $cor_class = 'bateria_moto';
                    break;
                    case 3:
                       $cor_class = 'bateria_barco';
                    break;
                    case 4:
                       $cor_class = 'bateria_lampada';
                    break;
                    case 5:
                       $cor_class = 'bateria_baterias';
                    break;

                    default:
                      $cor_class = 'bateria_carro';
                    break;
                  }
                  $i++;
                ?>
                    <div class="col-xs-4 pt10 pb15 <?php echo $cor_class; ?>">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                        <img width="73" height="34" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
                        <div class="top5">
                          <h1><?php Util::imprime($row[titulo]); ?></h1>
                        </div>
                        </a>
                    </div>
                <?php
                }
              }
              ?>





              </div>
              <!-- ======================================================================= -->
              <!-- MENU CATEGORIAS    -->
              <!-- ======================================================================= -->
            </div>
          </div>


          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->
          <div class="container">
           <div class="row">
             <div class="col-xs-12">
               <div class="contatosfale text-center">
                  <ul>
                    <li class="col-xs-4 padding0  active">
                      <a class="right10">OS MAIS VENDIDOS</a>
                    </li>

                    <li class="col-xs-4 padding0">
                      <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">OS MAIS VISITADOS</a>
                    </li>

                    <li class="col-xs-4 padding0 ">
                      <a href="javascript:;" id="scrollToBottom">EM PROMOÇÃO</a>
                    </li>
                  </ul>

              </div>

           </div>
         </div>
         </div>
         <!--  ==============================================================  -->
         <!-- MENU-->
         <!--  ==============================================================  -->


         <!--  ==============================================================  -->
         <!-- produto home-->
         <!--  ==============================================================  -->
         <div class="container">
           <div class="row top25">

             <?php
             $result = $obj_site->select("tb_produtos", "and exibir_home = 'SIM' order by rand() limit 4");
             if (mysql_num_rows($result) > 0) {
               $i = 0;
               while ($row = mysql_fetch_array($result)) {
               ?>
                 <div class="col-xs-6">
                   <div class="thumbnail produtos-home">

                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>">

                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 169, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>


                    <!--  ==============================================================  -->
                    <!-- TEXT DIAGONAL-->
                    <!--  ==============================================================  -->
                    <div class="diagonal">
                      <h3><?php Util::imprime($row[garantia]); ?></h3>
                    </div>
                    <!--  ==============================================================  -->
                    <!-- TEXT DIAGONAL-->
                    <!--  ==============================================================  -->

                    <div class="col-xs-12 padding0">
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_star_produto.png" alt="" />
                    </div>


                    <div class="caption top10">
                      <h1><?php Util::imprime($row[titulo]); ?></h1>
                        <div class="top10">
                        <p><?php Util::imprime($row[descricao],500); ?></p>
                        <div class="top15">
                            <h2>Ampere : <?php Util::imprime($row[codigo]); ?></h2>
                        </div>
                      </div>
                    </div>
                      </a>
                  </div>

                </div>
               <?php
                 if($i == 3){
                   echo '<div class="clearfix"></div>';
                   $i = 0;
                 }else{
                   $i++;
                 }

               }
             }
             ?>

           </div>
         </div>
         <!--  ==============================================================  -->
         <!-- produto home-->
         <!--  ==============================================================  -->





       <div class="container bg_conhecar_mais top60">
         <div class="row">
           <!--  ==============================================================  -->
           <!-- CONHECA MAIS-->
           <!--  ==============================================================  -->
           <div class="col-xs-12 top30">
               <h5>CONHEÇA O <span>SHOPPING DAS BATERIAS</span></h5>
           </div>
           <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
           <div class="col-xs-8 saiba_descricao top30">
           <p>
             <?php Util::imprime($row[descricao], 500); ?>
           </p>

         </div>
         <div class="col-xs-12 top5 bottom25">
           <a class="btn btn_transparente " href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"
             title="SAIBA MAIS">
             SAIBA MAIS
           </a>

         </div>

         <div class="col-xs-12 servicos_home text-center bottom20">
            <h1>NOSSOS SERVIÇOS</h1>
         </div>
         <!--  ==============================================================  -->
         <!-- CONHECA MAIS-->
         <!--  ==============================================================  -->

       <div class="clearfix"></div>


         <!--  ==============================================================  -->
         <!-- NOSSOS  SERVICOS-->
         <!--  ==============================================================  -->
         <?php
         $result = $obj_site->select("tb_servicos", "order by rand() limit 4");
         if (mysql_num_rows($result) > 0) {
           $i = 0;
           while($row = mysql_fetch_array($result)){
             ?>

         <div class="col-xs-12">
           <div class="media bg_servicos pt15">
             <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
             <div class="media-left media-middle pl5">
               <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 44, 41, array("class"=>"media-object", "alt"=>"$row[imagem]")) ?>
             </div>
             <div class="media-body">
               <h1 class="media-heading"><?php Util::imprime($row[titulo]); ?></h1>
                 <p><?php Util::imprime($row[descricao],200); ?></p>
             </div>
            </a>
           </div>
         </div>

         <?php
         if ($i == 1) {
           echo '<div class="clearfix"></div>';
           $i = 0;
         }else{
           $i++;
         }
       }
     }
     ?>
     <!--  ==============================================================  -->
     <!-- NOSSOS  SERVICOS-->
     <!--  ==============================================================  -->





        <div class="clearfix"></div>
        <div class="top80"></div>
         <!--  ==============================================================  -->
           <!--NOSSAS MARCAS-->
         <!--  ==============================================================  -->
        <?php require_once("./includes/marcas.php"); ?>
         <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
        <!--  ==============================================================  -->

       </div>
     </div>





  <?php require_once('./includes/rodape.php'); ?>




</body>

</html>




  <?php require_once('./includes/js_css.php'); ?>

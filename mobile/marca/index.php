
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 249px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


 <div class="container">
   <div class="row">
     <div class="col-xs-12">
       <!-- ======================================================================= -->
       <!-- Breadcrumbs    -->
       <!-- ======================================================================= -->
         <div class="breadcrumb">
           <a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
           <a class="active">Produtos</a>
           </div>
           <!-- ======================================================================= -->
           <!-- Breadcrumbs    -->
           <!-- ======================================================================= -->
     </div>
 </div>
 </div>


 <div class="container">
   <div class="row top15">


    <div class="col-xs-12">

      <!--  ==============================================================  -->
      <!--  CARROUCEL PRODUTOS-->
      <!--  ==============================================================  -->
      <div id="carousel-example-generic" class="carousel slide banners-interna-produtos" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators navs_produtos">
            <?php
            $result = $obj_site->select("tb_banners", "and tipo_banner = 4 order by rand() limit 3 ");
            if(mysql_num_rows($result) > 0){
              $i = 0;
              while ($row = mysql_fetch_array($result)) {
                $imagens[] = $row;
                ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                <?php
                $i++;
              }
            }
            ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

            <?php
            if (count($imagens) > 0) {
              $i = 0;
              foreach ($imagens as $key => $imagem) {
                ?>
                <div class="item <?php if($i == 0){ echo "active"; } ?>">

                  <?php if (!empty($imagem[url])): ?>
                    <a href="<?php Util::imprime($imagem[url]); ?>" >
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                    </a>
                  <?php else: ?>
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  <?php endif; ?>

                </div>
                <?php
                $i++;
              }
            }
            ?>


          </div>

          <!-- Controls -->
          <a class="left carousel-control controles_produtos" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control controles_produtos" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <!--  ==============================================================  -->
        <!--  CARROUCEL PRODUTOS-->
        <!--  ==============================================================  -->


        <a href="" title="">

        </a>

        <!--  ==============================================================  -->
        <!--  FILTROS CATEGORIAS -->
        <!--  ==============================================================  -->
        <div class="container">
            <div class="row top60">
                <div class="col-xs-12">


                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn_pesquisa_avancada input100" data-toggle="modal" data-target="#myModal_filtro_marca">
                      FILTRAR POR MARCA
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal_filtro_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Selecione a opção desejada</h4>
                          </div>
                          <div class="modal-body">
                              <?php require_once("../includes/filtro_marcas.php"); ?>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                          </div>
                        </div>
                      </div>
                    </div>





                </div>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!--  FILTROS CATEGORIAS -->
        <!--  ==============================================================  -->





       <!--  ==============================================================  -->
       <!--  TITULO E ORDERNAR -->
       <!--  ==============================================================  -->
       <div class="container">
            <div class="row top30">
                <div class="col-xs-6 top5 prod-title">
                    <h2>PRODUTOS</h2>
                </div>
                <div class="col-xs-6">
                    <form action="" method="post" >
                        <select name="ordem_produtos" id="" class="input100 select-transparente" onchange="this.form.submit()">
                            <option value="">ORDERNAR POR</option>
                            <option value="mais-recentes">MAIS RECENTES</option>
                            <option value="mais-vistos">MAIS VISTOS</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
       <!--  ==============================================================  -->
       <!--  TITULO E ORDERNAR -->
       <!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- produto home-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row top25">


    <?php
    //  FILTRA PELO TITULO
    if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
      $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
    endif;


    $get = explode("/", $_GET[get1]);

     //  FILTRA PELA MARCA
    $url1 = $get[0];
    if (isset( $url1 )) {
        $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1 );
        $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
    }


   //  FILTRA PELO TIPO DE VEÍCULO
    $url2 = $get[1];
    if (isset( $url2 )) {
        $id_categoria = $obj_site->get_id_url_amigavel("tb_tipos_veiculos", "idtipoveiculo", $url2 );
        $complemento .= "AND id_tipoveiculo = '$id_categoria' ";
    }



    // ORDEM DOS PRODUTOS
    switch ($_POST[ordem_produtos]) {
      case 'mais-vistos':
        $complemento .= "order by qtd_visitas desc";
      break;
      case 'mais-recentes':
        $complemento .= "order by idproduto desc";
      break;


    }




    $result = $obj_site->select("tb_produtos", $complemento);


    require_once("../includes/lista_produtos.php");



    ?>

  </div>
</div>
<!--  ==============================================================  -->
<!-- produto home-->
<!--  ==============================================================  -->








<!--  ==============================================================  -->
<!--NOSSAS MARCAS-->
<!--  ==============================================================  -->
 <div class="container">
    <div class="row top60">
        <?php require_once("../includes/marcas.php"); ?>
    </div>
</div>
<!--  ==============================================================  -->
<!--NOSSAS MARCAS-->
<!--  ==============================================================  -->



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>

<?php require_once('../includes/js_css.php'); ?>

<script>
  $(document).ready(function() {
    $('.FormContatos').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      localidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>

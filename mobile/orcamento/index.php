<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}




?>
    <!doctype html>
    <html>

    <head>
        <?php require_once('.././includes/head.php'); ?>



    </head>

    <!--  ==============================================================  -->
    <!-- background -->
    <!--  ==============================================================  -->
    <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17) ?>
        <style>
            .bg-interna {
                background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 247px center no-repeat;
            }

        </style>

        <body class="bg-interna">


            <!-- ======================================================================= -->
            <!-- topo    -->
            <!-- ======================================================================= -->
            <?php require_once('../includes/topo.php') ?>
            <!-- ======================================================================= -->
            <!-- topo    -->
            <!-- ======================================================================= -->










                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- ======================================================================= -->
                                <!-- Breadcrumbs    -->
                                <!-- ======================================================================= -->
                                <div class="breadcrumb">
                                    <a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
                                    <a class="active">Orçamentos</a>
                                </div>
                                <!-- ======================================================================= -->
                                <!-- Breadcrumbs    -->
                                <!-- ======================================================================= -->
                            </div>

                            <div class="col-xs-12 descricao_banner_orcamento top55 text-center">
                                <h1>MEU ORÇAMENTO</h1>
                            </div>

                        </div>
                    </div>


                    <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
                        <div class="container">
                            <div class="row top50">

                                <!-- ======================================================================= -->
                                <!-- CARRINHO  -->
                                <!-- ======================================================================= -->
                                <div class="col-xs-12 tabela_carrinho top20">
                                    <h4><span>ITENS SELECIONADOS</span></h4>

                                    <table class="table table-condensed col-xs-12 top40">
                                        <tbody>


                                           <?php
                                            if(count($_SESSION[solicitacoes_produtos]) > 0){
                                              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                                                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                                ?>
                                                <tr>
                                                  <td>
                                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                                  </td>
                                                  <td class="col-xs-6"><?php Util::imprime($row[titulo]); ?></td>
                                                  <td class="text-center">
                                                    QDD.<br>
                                                      <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                                                  </td>
                                                  <td class="text-center">
                                                    <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                                                      <i class="fa fa-times-circle fa-2x top20"></i>
                                                    </a>
                                                  </td>
                                                </tr>
                                              <?php
                                              }
                                            }
                                            ?>



                                        </tbody>
                                    </table>
                                </div>
                                <!-- ======================================================================= -->
                                <!-- CARRINHO  -->
                                <!-- ======================================================================= -->


                                <div class="col-xs-12 top15 padding0">






                                        <div class="fundo_formulario">
                                            <!-- formulario orcamento -->

                                            <div class="col-xs-12 top10">
                                                <div class="form-group input100 has-feedback">
                                                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                                                    <span class="fa fa-user form-control-feedback top15"></span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 top10">
                                                <div class="form-group  input100 has-feedback">
                                                    <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                                                    <span class="fa fa-envelope form-control-feedback top15"></span>
                                                </div>
                                            </div>


                                            <div class="col-xs-12 top10">
                                                <div class="form-group  input100 has-feedback">
                                                    <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                                                    <span class="fa fa-phone form-control-feedback top15"></span>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 top10">
                                                <div class="form-group  input100 has-feedback">
                                                    <input type="text" name="" class="form-control fundo-form1 input-lg input100" placeholder="LOCALIDADE">
                                                    <span class="fa fa-home form-control-feedback"></span>
                                                </div>
                                            </div>


                                            <div class="col-xs-12 top10">
                                                <div class="form-group input100 has-feedback">
                                                    <textarea name="" cols="30" rows="5" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                                                    <span class="fa fa-pencil form-control-feedback top15"></span>
                                                </div>
                                            </div>


                                            <!-- formulario orcamento -->
                                            <div class="col-xs-12 top10 text-right">
                                                <div class="top15 bottom25">
                                                    <button type="submit" class="btn btn-formulario" name="btn_contato">
                                                        ENVIAR
                                                    </button>
                                                </div>
                                            </div>

                                        </div>


                                        <!--  ==============================================================  -->
                                        <!-- FORMULARIO-->
                                        <!--  ==============================================================  -->


                                </div>



                            </div>
                        </div>


                    </form>





                    <div class="container">
                        <div class="row top60">
                            <!--  ==============================================================  -->
                            <!--NOSSAS MARCAS-->
                            <!--  ==============================================================  -->
                            <?php require_once('../includes/marcas.php'); ?>
                            <!--  ==============================================================  -->
                            <!--NOSSAS MARCAS-->
                            <!--  ==============================================================  -->
                        </div>
                    </div>






                    <?php require_once('../includes/rodape.php'); ?>

        </body>

    </html>

<?php require_once('../includes/js_css.php'); ?>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

//  CADASTRO OS PRODUTOS SOLICITADOS
for($i=0; $i < count($_POST[qtd]); $i++){
$dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

$produtos .= "
<tr>
<td><p>". $_POST[qtd][$i] ."</p></td>
<td><p>". utf8_encode($dados[titulo]) ."</p></td>
</tr>
";
}


$texto_mensagem = "
O seguinte cliente fez uma solicitação pelo site. <br />

Nome: $_POST[nome] <br />
Email: $_POST[email] <br />
Telefone: $_POST[telefone] <br />
Localidade: $_POST[localidade] <br />
Mensagem: <br />
". nl2br($_POST[mensagem]) ." <br />

<br />
<h2> Produtos selecionados:</h2> <br />

<table width='100%' border='0' cellpadding='5' cellspacing='5'>
<tr>
    <td><h4>QTD</h4></td>
    <td><h4>PRODUTO</h4></td>
</tr>
$produtos
</table>
";
Util::envia_email($config[email], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
Util::envia_email($config[email_copia], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
Util::alert_bootstrap("Obrigado por entrar em contato.");
unset($_POST);
}
?>



    <script>
        $(document).ready(function() {
            $('.FormContatos').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    nome: {
                        validators: {
                            notEmpty: {

                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {

                            },
                            emailAddress: {
                                message: 'Esse endereço de email não é válido'
                            }
                        }
                    },
                    telefone: {
                        validators: {
                            notEmpty: {

                            },
                            phone: {
                                country: 'BR',
                                message: 'Informe um telefone válido.'
                            }
                        },
                    },
                    assunto: {
                        validators: {
                            notEmpty: {

                            }
                        }
                    },
                    localidade: {
                        validators: {
                            notEmpty: {

                            }
                        }
                    },
                    mensagem: {
                        validators: {
                            notEmpty: {

                            }
                        }
                    }
                }
            });
        });

    </script>


<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 247px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


 <div class="container">
   <div class="row">
     <div class="col-xs-12">
       <!-- ======================================================================= -->
       <!-- Breadcrumbs    -->
       <!-- ======================================================================= -->
         <div class="breadcrumb">
           <a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
           <a class="active">Serviços</a>
           </div>
           <!-- ======================================================================= -->
           <!-- Breadcrumbs    -->
           <!-- ======================================================================= -->
     </div>
 </div>
 </div>



<div class="container">
  <div class="row top50 div-lista-servicos">
    <!-- ======================================================================= -->
    <!-- SERVICO 01  -->
    <!-- ======================================================================= -->
    <?php
    $result = $obj_site->select("tb_servicos","order by rand() limit 4");
    if (mysql_num_rows($result) > 0) {
      $i = 0;
      while($row = mysql_fetch_array($result)){
        ?>

    <div class="col-xs-10 top25">
     <div class="tipos_servicos">
       <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
       <div class=" top10 pg10">
       <h1><?php Util::imprime($row[titulo]); ?></h1>
       </div>

       <div class="pg10">
       <p>
         <?php Util::imprime($row[descricao],200); ?>
       </p>
       </div>
       </a>
     </div>
    </div>

    <?php
    if ($i == 1) {
      echo '<div class="clearfix"></div>';
      $i = 0;
    }else{
      $i++;
    }
  }
}
  ?>




</div>
</div>



 <div class="container">
  <div class="row top30">
      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->
      <?php require_once("../includes/marcas.php") ?>
      <!--  ==============================================================  -->
     <!--NOSSAS MARCAS-->
     <!--  ==============================================================  -->
 </div>
 </div>






<?php require_once('../includes/rodape.php'); ?>

</body>

</html>

<?php require_once('../includes/js_css.php'); ?>

<script>
  $(document).ready(function() {
    $('.FormContatos').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
			 notEmpty: {

             },
		   phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      localidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>

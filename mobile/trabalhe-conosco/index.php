<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 247px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


 <div class="container">
   <div class="row">
     <div class="col-xs-12">
       <!-- ======================================================================= -->
       <!-- Breadcrumbs    -->
       <!-- ======================================================================= -->
         <div class="breadcrumb">
           <a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
           <a href="<?php echo Util:: caminho_projeto() ?>/mobile/contatos">Contatos</a>
           <a class="active">Trabalhe Conosco</a>
           </div>
           <!-- ======================================================================= -->
           <!-- Breadcrumbs    -->
           <!-- ======================================================================= -->
     </div>
  </div>
 </div>


 <!--  ==============================================================  -->
 <!-- MENU-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row top120">
    <div class="col-xs-12">
      <div class="contatosfale text-center">
         <ul>
           <li class="col-xs-4 padding0">
             <a  href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco" class="right30">FALE CONOSCO</a>
           </li>

           <li class="col-xs-4 padding0 active">
             <a>TRABALHE CONOSCO</a>
           </li>

           <li class="col-xs-4 padding0">
             <a class="left30" href="javascript:;" id="scrollToBottom">COMO CHEGAR</a>
           </li>
         </ul>

     </div>

  </div>
</div>
</div>
<!--  ==============================================================  -->
<!-- MENU-->
<!--  ==============================================================  -->


<div class="container ">
 <div class="row">


   <div class="col-xs-12 top15 padding0">



       <form class="form-inline Curriculo_trabalhe" role="form" method="post" enctype="multipart/form-data">
         <div class="fundo_formulario">
           <!-- formulario orcamento -->

             <div class="col-xs-12 top10">
               <div class="form-group input100 has-feedback">
                 <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                 <span class="fa fa-user form-control-feedback top15"></span>
               </div>
             </div>

             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                 <span class="fa fa-phone form-control-feedback top15"></span>
               </div>
             </div>


             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="area" class="form-control fundo-form1 input-lg input100" placeholder="AREA">
                 <span class="fa fa-globe form-control-feedback"></span>
               </div>
             </div>

             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="cidade" class="form-control fundo-form1 input-lg input100" placeholder="CIDADE">
                 <span class="fa fa-globe form-control-feedback top15"></span>
               </div>
             </div>


             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                 <span class="fa fa-envelope form-control-feedback top15"></span>
               </div>
             </div>

             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="escolaridade" class="form-control fundo-form1 input-lg input100" placeholder="ESCOLARIDADE">
                 <span class="glyphicon glyphicon-book form-control-feedback top5"></span>
               </div>
             </div>

             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="cargo" class="form-control fundo-form1 input-lg input100" placeholder="CARGO">
                 <span class="glyphicon glyphicon-lock form-control-feedback top5"></span>
               </div>
             </div>

             <div class="col-xs-12 top10">
               <div class="form-group  input100 has-feedback">
                 <input type="text" name="estado" class="form-control fundo-form1 input-lg input100" placeholder="ESTADO">
                 <span class="fa fa-globe form-control-feedback top15"></span>
               </div>
             </div>


           <div class="col-xs-12 top10">
             <div class="form-group input100 has-feedback ">
               <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
               <span class="glyphicon glyphicon-file form-control-feedback top10"></span>
             </div>
           </div>

           <div class="col-xs-12 top10">
            <div class="form-group input100 has-feedback">
             <textarea name="mensagem" cols="30" rows="5" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
             <span class="fa fa-pencil form-control-feedback top15"></span>
           </div>
         </div>


         <!-- formulario orcamento -->
         <div class="col-xs-12 top10 text-right">
           <div class="top15 bottom25">
             <button type="submit" class="btn btn-formulario" name="btn_contato">
               ENVIAR
             </button>
           </div>
         </div>

       </div>


       <!--  ==============================================================  -->
       <!-- FORMULARIO-->
       <!--  ==============================================================  -->
     </form>

 </div>





 </div>
</div>





 <div class="container">
   <div class="row top35">

     <div class="col-xs-12">
           <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_chegar_mais.png" alt="">
     </div>

        <div class="col-xs-12">
          <div class="top40 bottom20">
            <p><?php Util::imprime($config[endereco]); ?></p>
          </div>
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="489" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

         <div class="clearfix">  </div>
          <div class="top80"></div>
           <!--  ==============================================================  -->
             <!--NOSSAS MARCAS-->
           <!--  ==============================================================  -->
           <?php require_once("../includes/marcas.php"); ?>
           <!--  ==============================================================  -->
          <!--NOSSAS MARCAS-->
          <!--  ==============================================================  -->


      </div>
  </div>








<?php require_once('../includes/rodape.php'); ?>

</body>

</html>

<?php require_once('../includes/js_css.php'); ?>


<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

if(!empty($_FILES[curriculo][name])):
  $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
  $texto = "Anexo: ";
  $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
  $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
endif;

	  $texto_mensagem = "
						Nome: ".$_POST[nome]." <br />
						Telefone: ".$_POST[telefone]." <br />
						Email: ".$_POST[email]." <br />
						Escolaridade: ".$_POST[escolaridade]." <br />
						Cargo: ".$_POST[cargo]." <br />
						Área: ".$_POST[area]." <br />
						Cidade: ".$_POST[cidade]." <br />
						Estado: ".$_POST[estado]." <br />
						Mensagem: <br />
						".nl2br($_POST[mensagem])."

						<br><br>
						$texto
						";


	  Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
	  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
	  Util::alert_bootstrap("Obrigado por entrar em contato.");
	  unset($_POST);
}

?>



<script>
  $(document).ready(function() {
    $('.Curriculo_trabalhe').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
			 notEmpty: {

             },
			 phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

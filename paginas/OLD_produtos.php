<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>




</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  148px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">

      <div class="col-xs-8">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb top15">
          	<a href="<?php echo Util:: caminho_projeto() ?>/"><i class="fa fa-home"></i></a>
            <a class="active">Produtos</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/contatos.php') ?>
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->

  </div>
 </div>



     <div class="container">
       <div class="row top35">

          <!--  ==============================================================  -->
           <!--  MENU lateral-->
          <!--  ==============================================================  -->
         <div class="col-xs-3 bg_branco pt20">
             <?php require_once("./includes/menu_produtos.php") ?>
        </div>
        <!--  ==============================================================  -->
        <!--  MENU lateral-->
        <!--  ==============================================================  -->



        <div class="col-xs-9">

          <!--  ==============================================================  -->
          <!--  CARROUCEL PRODUTOS-->
          <!--  ==============================================================  -->
          <div id="carousel-example-generic" class="carousel slide bottom70" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators navs_produtos">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 3 order by rand() limit 3 ");
                if(mysql_num_rows($result) > 0){
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                    <?php
                    $i++;
                  }
                }
                ?>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <?php
                if (count($imagens) > 0) {
                  $i = 0;
                  foreach ($imagens as $key => $imagem) {
                    ?>
                    <div class="item <?php if($i == 0){ echo "active"; } ?>">

                      <?php if (!empty($imagem[url])): ?>
                        <a href="<?php Util::imprime($imagem[url]); ?>" >
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                        </a>
                      <?php else: ?>
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                      <?php endif; ?>

                    </div>
                    <?php
                    $i++;
                  }
                }
                ?>
              </div>

              <!-- Controls -->
              <a class="left carousel-control controles_produtos" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control controles_produtos" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <!--  ==============================================================  -->
            <!--  CARROUCEL PRODUTOS-->
            <!--  ==============================================================  -->


            <!--  ==============================================================  -->
            <!--  QUANTIDADE PRODUTOS-->
            <!--  ==============================================================  -->

            <div class="col-xs-5 quantidades">
                <h1>PRODUTOS</h1>
            </div>
            <!--  ==============================================================  -->
            <!--  QUANTIDADE PRODUTOS-->
            <!--  ==============================================================  -->



            <!--  ==============================================================  -->
      	    <!-- BARRA CATEGORIA ORDENA POR -->
      	    <!--  ==============================================================  -->
      	    <div class="col-xs-7 menu_produtos">
      	      <form name="form_ordem" action="" method="post">
                <select name="ordem_produtos" class="selectpicker" data-live-search="false" title="ORDENAR POR" data-width="100%" onchange="this.form.submit()" >
        	        <optgroup label="SELECIONE">
        	              <option value="mais-recentes" <?php if($_POST[ordem_produtos] == "mais-recentes"){ echo "selected"; } ?> data-tokens="MAIS RECENTES">MAIS RECENTES</option>
        	              <option value="mais-vistos" <?php if($_POST[ordem_produtos] == "mais-vistos"){ echo "selected"; } ?> data-tokens="MAIS VISTOS">MAIS VISTOS</option>
        	          </optgroup>
        	        </select>
                </form>
      	      </div>
      	    <!--  ==============================================================  -->
      	    <!-- BARRA CATEGORIA ORDENA POR -->
      	    <!--  ==============================================================  -->



            <div class="clearfix">  </div>


            <!--  ==============================================================  -->
            <!-- produto home-->
            <!--  ==============================================================  -->
              <div class="top35">


                    <?php

                    //  FILTRA PELO TIPO DE VEÍCULO
                    $url1 = Url::getURL(1);
                    if (isset( $url1 )) {
                        $id_categoria = $obj_site->get_id_url_amigavel("tb_tipos_veiculos", "idtipoveiculo", $url1 );
                        $complemento .= "AND tp.id_tipoveiculo = '$id_categoria' ";
                    }


                    //  FILTRA PELA MARCA
                    $url2 = Url::getURL(2);
                    if (isset( $url2 )) {
                        $id_categoria = $obj_site->get_id_url_amigavel("tb_marcas_veiculos", "idmarcaveiculo", $url2 );
                        $complemento .= "AND tpma.id_marcaveiculo = '$id_categoria' ";
                    }

                    //  FILTRA PELO MODELO
                    $url3 = Url::getURL(3);
                    if (isset( $url3 )) {
                        $id_categoria = $obj_site->get_id_url_amigavel("tb_modelos_veiculos", "idmodeloveiculo", $url3 );
                        $complemento .= "AND tpma.id_modeloveiculo = '$id_categoria' ";
                    }


                    //  FILTRA PELO ANO
                    $url4 = Url::getURL(4);
                    if (isset( $url4 )) {
                        $complemento .= "AND '$url4' BETWEEN tpma.ano_inicial and tpma.ano_final ";
                    }






                    //  FILTRA PELO TITULO
                    if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
                      $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
                    endif;



                    // ORDEM DOS PRODUTOS
                    switch ($_POST[ordem_produtos]) {
                      case 'mais-vistos':
                        $complemento .= "order by qtd_visitas desc";
                      break;
                      case 'mais-recentes':
                        $complemento .= "order by idproduto desc";
                      break;


                    }



                    // verifica se tem filtro ou nao
                    if(isset($url1) and !empty($url1) and !isset($url2) and empty($url2)){
                      $sql = "
                              select
                                *
                              from
                                tb_produtos tp
                              WHERE
                                tp.ativo = 'SIM'
                                $complemento
                                group by idproduto
                              ";
                      $result = $obj_site->executaSQL($sql);
                  }elseif(isset($url1) and !empty($url1) and isset($url2) and !empty($url2)){
                        $sql = "
                              select
                                *
                              from
                                tb_produtos tp, tb_produtos_modelos_aceitos tpma
                              WHERE
                                tp.idproduto = tpma.id_produto
                                AND tp.ativo = 'SIM'
                                $complemento
                                group by id_produto
                              ";
                       $result = $obj_site->executaSQL($sql);
                    }else{
                      $result = $obj_site->select("tb_produtos", $complemento);
                    }

                    //  LISTA OS PRODUTOS
                    require_once("./includes/lista_produtos.php");
                    ?>



















              </div>
            <!--  ==============================================================  -->
            <!-- produto home-->
            <!--  ==============================================================  -->




        </div>


     </div>
  </div>

  <div class="container">
    <div class="row">
      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->
      <?php require_once("./includes/marcas.php"); ?>

      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->
    </div>
  </div>





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->

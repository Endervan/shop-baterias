<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  148px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row top25">

      <div class="col-xs-8">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb top15">
          	<a href="<?php echo Util:: caminho_projeto() ?>/"><i class="fa fa-home"></i></a>
            <a class="active">Contatos</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/contatos.php') ?>
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->

  </div>
 </div>


 <!--  ==============================================================  -->
 <!-- MENU-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row top30">
    <div class="col-xs-8 col-xs-offset-4 ">
      <div class="contatosfale text-center">
         <ul>
           <li class="col-xs-4 padding0  active">
             <a class="right55">FALE CONOSCO</a>
           </li>

           <li class="col-xs-4 padding0">
             <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
           </li>

           <li class="col-xs-4 padding0 ">
             <a class="left55" href="javascript:void(0);" title="Para o topo" onclick="$('html,body').animate({scrollTop: $('.como-chegar').offset().top}, 2000);">
                 COMO CHEGAR
             </a>
           </li>
         </ul>

     </div>

  </div>
</div>
</div>
<!--  ==============================================================  -->
<!-- MENU-->
<!--  ==============================================================  -->



     <div class="container top40">
      <div class="row">
        <!--  ==============================================================  -->
        <!-- COMO CHEGAR-->
        <!--  ==============================================================  -->
      <div class="col-xs-4">
        <div class="top370">
          <div class="media">
            <div class="media-left media-middle">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_chegar.png" alt="">
            </div>
            <div class="media-body">
                <img class="media-heading" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_chegar_mais.png" alt="">
            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- COMO CHEGAR-->
        <!--  ==============================================================  -->



      </div>




        <div class="col-xs-8">





            <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
              <div class="fundo_formulario">
                <!-- formulario orcamento -->
                <div class="top20">
                  <div class="col-xs-6">
                    <div class="form-group input100 has-feedback">
                      <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                      <span class="fa fa-user form-control-feedback top15"></span>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="form-group  input100 has-feedback">
                      <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                      <span class="fa fa-envelope form-control-feedback top15"></span>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>


                <div class="top20">
                  <div class="col-xs-6">
                    <div class="form-group  input100 has-feedback">
                      <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                      <span class="fa fa-phone form-control-feedback top15"></span>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="form-group  input100 has-feedback">
                      <input type="text" name="localidade" class="form-control fundo-form1 input-lg input100" placeholder="LOCALIDADE">
                      <span class="fa fa-home form-control-feedback"></span>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>



                <div class="top15">
                  <div class="col-xs-12">
                   <div class="form-group input100 has-feedback">
                    <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback top15"></span>
                  </div>
                </div>
              </div>

              <!-- formulario orcamento -->
              <div class="col-xs-12 text-right">
                <div class="top15 bottom25">
                  <button type="submit" class="btn btn-formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>

            </div>


            <!--  ==============================================================  -->
            <!-- FORMULARIO-->
            <!--  ==============================================================  -->
          </form>

      </div>



      </div>
     </div>





<div class="clearfix">  </div>


  <div class="container">
    <div class="row">

     <div class="col-xs-12 padding0 bottom60 como-chegar">
        <div class="top40 bottom20">
            <p><?php Util::imprime($config[endereco]); ?></p>
          </div>
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>



      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->
      <?php require_once("./includes/marcas.php") ?>
      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->
    </div>
  </div>







<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->



<?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
       $texto_mensagem = "
                          Nome: ".($_POST[nome])." <br />
                          Localidade: ".($_POST[localidade])." <br />
                          Telefone: ".($_POST[telefone])." <br />
                          Email: ".($_POST[email])." <br />
                          Mensagem: <br />
                          ".(nl2br($_POST[mensagem]))."
                          ";
        Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
        Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
    }


    ?>



<script>
  $(document).ready(function() {
    $('.FormContatos').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
         validators: {
             notEmpty: {

             },
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>

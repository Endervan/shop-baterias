<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>




</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  148px center no-repeat;
  }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">

      <div class="col-xs-8">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb top15">
         <a href="<?php echo Util:: caminho_projeto() ?>/"><i class="fa fa-home"></i></a>
         <a href="<?php echo Util:: caminho_projeto() ?>/produtos">Produtos</a>
         <a class="active">Produtos Categorias</a>
       </div>
       <!-- ======================================================================= -->
       <!-- Breadcrumbs    -->
       <!-- ======================================================================= -->
     </div>

     <!-- ======================================================================= -->
     <!-- CONTATOS  -->
     <!-- ======================================================================= -->
     <?php require_once('./includes/contatos.php') ?>
     <!-- ======================================================================= -->
     <!-- CONTATOS  -->
     <!-- ======================================================================= -->

   </div>
 </div>



 <div class="container">
   <div class="row top35">

    <!--  ==============================================================  -->
    <!--  MENU lateral-->
    <!--  ==============================================================  -->
    <div class="col-xs-3 bg_branco pt20">
     <?php require_once("./includes/menu_produtos.php") ?>
   </div>
   <!--  ==============================================================  -->
   <!--  MENU lateral-->
   <!--  ==============================================================  -->



   <div class="col-xs-9">



    <!--  ==============================================================  -->
          <!--  CARROUCEL PRODUTOS-->
          <!--  ==============================================================  -->
          <div id="carousel-example-generic" class="carousel slide bottom70" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators navs_produtos">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 3 order by rand() limit 3 ");
                if(mysql_num_rows($result) > 0){
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                    ?>
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                    <?php
                    $i++;
                  }
                }
                ?>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <?php
                if (count($imagens) > 0) {
                  $i = 0;
                  foreach ($imagens as $key => $imagem) {
                    ?>
                    <div class="item <?php if($i == 0){ echo "active"; } ?>">

                      <?php if (!empty($imagem[url])): ?>
                        <a href="<?php Util::imprime($imagem[url]); ?>" >
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                        </a>
                      <?php else: ?>
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                      <?php endif; ?>

                    </div>
                    <?php
                    $i++;
                  }
                }
                ?>
              </div>

              <!-- Controls -->
              <a class="left carousel-control controles_produtos" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control controles_produtos" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <!--  ==============================================================  -->
            <!--  CARROUCEL PRODUTOS-->
            <!--  ==============================================================  -->



    <!--  ==============================================================  -->
            <!--  QUANTIDADE PRODUTOS-->
            <!--  ==============================================================  -->

            <div class="col-xs-5 quantidades top20">
                <h1>PRODUTOS</h1>
            </div>
            <!--  ==============================================================  -->
            <!--  QUANTIDADE PRODUTOS-->
            <!--  ==============================================================  -->



            <!--  ==============================================================  -->
            <!-- BARRA CATEGORIA ORDENA POR -->
            <!--  ==============================================================  -->
            <div class="col-xs-7 menu_produtos top20">
              <form name="form_ordem" action="" method="post">
                <select name="ordem_produtos" class="selectpicker" data-live-search="false" title="ORDENAR POR" data-width="100%" onchange="this.form.submit()" >
                  <optgroup label="SELECIONE">
                        <option value="mais-recentes" <?php if($_POST[ordem_produtos] == "mais-recentes"){ echo "selected"; } ?> data-tokens="MAIS RECENTES">MAIS RECENTES</option>
                        <option value="mais-vistos" <?php if($_POST[ordem_produtos] == "mais-vistos"){ echo "selected"; } ?> data-tokens="MAIS VISTOS">MAIS VISTOS</option>
                    </optgroup>
                  </select>
                </form>
              </div>
            <!--  ==============================================================  -->
            <!-- BARRA CATEGORIA ORDENA POR -->
            <!--  ==============================================================  -->



            <div class="clearfix">  </div>




   <!--  ==============================================================  -->
   <!-- produto home-->
   <!--  ==============================================================  -->
   <div class="top35">



  <?php
         $result = $obj_site->select("tb_produtos", "order by rand() limit 9");
         if (mysql_num_rows($result) > 0) {
           $i = 0;
           while ($row = mysql_fetch_array($result)) {
           ?>
             <div class="col-xs-4">
               <div class="thumbnail produtos-home">

                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 271, 218, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

                <div class="produto-hover">
                  <div class="col-xs-6 text-center">
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba mais">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.png" alt="" class="input100">
                    </a>
                  </div>
                  <div class="col-xs-6 hover-btn-orcamento">
                    <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.png" alt="" class="input100">
                    </a>
                  </div>

                </div>

                <!--  ==============================================================  -->
                <!-- TEXT DIAGONAL-->
                <!--  ==============================================================  -->
                <div class="diagonal">
                  <h3><?php Util::imprime($row[anos]); ?> ANOS</h3>
                </div>
                <!--  ==============================================================  -->
                <!-- TEXT DIAGONAL-->
                <!--  ==============================================================  -->

                <div class="col-xs-12 padding0">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_star_produto.png" alt="" />
                </div>


                <div class="caption top10">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <div class="top10">
                    <p><?php Util::imprime($row[descricao],500); ?></p>
                    <div class="top15">
                        <h2>Tipo de Polo: <?php Util::imprime($row[codigo]); ?></h2>
                    </div>
                  </div>
                </div>
              </div>

            </div>
           <?php
             if($i == 2){
               echo '<div class="clearfix"></div>';
               $i = 0;
             }else{
               $i++;
             }

           }
         }
         ?>
      </div>
      <!--  ==============================================================  -->
      <!-- produto home-->
      <!--  ==============================================================  -->

      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <!--  ==============================================================  -->
    <!--NOSSAS MARCAS-->
    <!--  ==============================================================  -->
    <div class="col-xs-2">
      <a href="<?php echo Util::caminho_projeto() ?>/servicos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas01.png" alt="" />
      </a>
    </div>

    <div class="col-xs-2">
      <a href="<?php echo Util::caminho_projeto() ?>/servicos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas02.png" alt="" />
      </a>
    </div>

    <div class="col-xs-2">
      <a href="<?php echo Util::caminho_projeto() ?>/servicos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas03.png" alt="" />
      </a>
    </div>

    <div class="col-xs-2">
      <a href="<?php echo Util::caminho_projeto() ?>/servicos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas04.png" alt="" />
      </a>
    </div>

    <div class="col-xs-2">
      <a href="<?php echo Util::caminho_projeto() ?>/servicos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas02.png" alt="" />
      </a>
    </div>

    <div class="col-xs-2">
      <a href="<?php echo Util::caminho_projeto() ?>/servicos">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas03.png" alt="" />
      </a>
    </div>
    <!--  ==============================================================  -->
    <!--NOSSAS MARCAS-->
    <!--  ==============================================================  -->
  </div>
</div>





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->

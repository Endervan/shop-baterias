<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  148px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">

      <div class="col-xs-8">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb top15">
          	<a href="<?php echo Util:: caminho_projeto() ?>/"><i class="fa fa-home"></i></a>
            <a class="active">Serviços</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/contatos.php') ?>
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->

  </div>
 </div>



     <div class="container">
       <div class="row top20">
         <!-- ======================================================================= -->
         <!-- SERVICO 01  -->
         <!-- ======================================================================= -->
         <?php
         $result = $obj_site->select("tb_servicos");
         if (mysql_num_rows($result) > 0) {
           $i = 0;
           while($row = mysql_fetch_array($result)){
             ?>


         <div class="col-xs-5 top40">
          <div class="tipos_servicos">
              <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class=" top10 pg10">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>

            <div class="pg10">
            <p>
              <?php Util::imprime($row[descricao], 200); ?>
            </p>
            </div>
            </a>
          </div>
         </div>

         <?php
         if ($i == 1) {
           echo '<div class="clearfix"></div>';
           $i = 0;
         }else{
           $i++;
         }
       }
     }
       ?>

    </div>
  </div>





  <div class="container-fluid top75 bg_branco">
  <div class="row">


    <div class="container ">
    <div class="row ">

      <div class="top30">  </div>
      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->
      <?php require_once("./includes/marcas.php"); ?>
      <!--  ==============================================================  -->
        <!--NOSSAS MARCAS-->
      <!--  ==============================================================  -->

    </div>
    </div>


  </div>
  </div>








<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>



<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->


<style media="screen">
    .rodape{margin-top: 0px;}
</style>
